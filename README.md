# Odin

**Odin** is an decentralized peer-to-peer application via the **libp2p**
network stack. For sharing data, a **pns://** URI is provided, which consists basically of a peer
id, which enables others to find your running server on the network.

## General

The basic characteristics of the app are decentralized, respect of personal data,
open source, free of charge, transparent, free of advertising and legally impeccable.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/threads.server/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=threads.server)

## Documentation

**Odin** is a server only implementation. The related client application can be found at the
following location [Thor](https://gitlab.com/lp2p/thor).

The reason for the strict separation between client and server, relies in performance reasons.

The client **Thor** is available in several app stores (like F-Droid, Google Play).

**Odin** based on a subset of [libp2p](https://github.com/libp2p/specs/tree/master) which are described in detail in
the [Lite](https://gitlab.com/lp2p/lite/) library.

The application itself requires the **IPv6** protocol to function. When you are behind a router you
might be able to switch to the **IPv6** protocol and make the necessary settings, so that your
service is globally reachable (Port Forwarding).

### Port Forwarding

Port forwarding allows you to specify that all communications received on a certain port are
forwarded directly to your device. You set port forwarding rules up on your router. The following
list provides information which might be required for the port forwarding.

- **Interface** IPv6
  Note: Only IPv6 is supported, because a direct connection between two peers can only be
  established when they use the same IP interface
- **Port** 5001
- **Protocol** TCP
  Note: The protocol is TCP. The direct connection between two peers will be established via a
  secure TCP connection, protected by a self signed **libp2p** certificate.
- **Application** Odin
  Note: Usually can be any name

### Concept

The application tries to connect to **libp2p** relay peers, so that it can be found by other peers
via an unique identifier. This peer identifier (<pid>) is also used for marking the content via the
**pns://<pid>/<cid>** URI. (Note: <cid> is a content identifier)

### Notes

The current implementation of **Odin** is not compatible to **IPFS**. Therefore a regular **IPFS**
node, will not find any **Odin** data.

With this in mind the, a client [Thor](https://gitlab.com/lp2p/thor) has been developed to show the
content of the **Odin** service.

## Links

[Privacy Policy](https://gitlab.com/lp2p/odin/-/blob/master/POLICY.md)
<br/>
[Apache License](https://gitlab.com/lp2p/odin/-/blob/master/LICENSE)
