package threads.odin

import android.app.Application
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import tech.lp2p.odin.createDataStore
import tech.lp2p.odin.createFiles
import tech.lp2p.odin.data.Files
import threads.odin.model.API
import threads.odin.model.api
import threads.odin.model.debug
import threads.odin.work.InitPageWorker
import kotlin.time.measureTime

class App : Application() {
    private var datastore: DataStore<Preferences>? = null
    private var files: Files? = null

    fun datastore(): DataStore<Preferences> {
        return datastore!!
    }

    fun files(): Files {
        return files!!
    }


    override fun onCreate() {
        super.onCreate()

        val time = measureTime {
            datastore = createDataStore(applicationContext)
            files = createFiles(applicationContext)


            val api: API = api(applicationContext)
            api.runService()

            kotlinx.coroutines.MainScope().launch { // todo maybe
                delay(5000) // 5 sec initial delay
                while (isActive) {
                    api.hopReserve()
                    delay((60 * 30 * 1000).toLong()) // 30 min
                }
            }
        }
        InitPageWorker.initPage(applicationContext)
        debug("App", "App started " + time.inWholeMilliseconds)
    }
}