package threads.odin.model

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdManager.DiscoveryListener
import android.net.nsd.NsdManager.RegistrationListener
import android.net.nsd.NsdManager.ServiceInfoCallback
import android.net.nsd.NsdServiceInfo
import android.os.Build
import androidx.annotation.RequiresApi
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.Dark
import tech.lp2p.lite.createPeeraddr
import tech.lp2p.lite.decodePeerId
import java.net.InetAddress
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import kotlin.io.encoding.ExperimentalEncodingApi

@Suppress("unused")
class MDNS private constructor(private val nsdManager: NsdManager, private val mdnsName: String) {
    private val registrationService: RegistrationService

    @Volatile
    private var discoveryService: DiscoveryService? = null

    @Volatile
    private var serviceStarted = false

    init {
        this.registrationService = RegistrationService()
    }

    @OptIn(ExperimentalEncodingApi::class)
    fun startService(dark: Dark, name: String, icon: Long?) {
        val peerId = dark.peerId()

        val serviceInfo = NsdServiceInfo()

        serviceInfo.serviceName = peerId.toBase58()
        serviceInfo.serviceType = mdnsName
        serviceInfo.port = ODIN_PORT
        serviceInfo.setAttribute("name", name)
        if (icon != null) {
            serviceInfo.setAttribute("icon", icon.toString())
        }

        nsdManager.registerService(serviceInfo, NsdManager.PROTOCOL_DNS_SD, registrationService)

        serviceStarted = true
    }

    fun startDiscovery(consumer: (Peeraddr, String) -> Unit) {
        try {
            this.discoveryService = DiscoveryService(nsdManager, consumer)

            nsdManager.discoverServices(
                mdnsName,
                NsdManager.PROTOCOL_DNS_SD, discoveryService
            )
        } catch (throwable: Throwable) {
            debug(TAG, throwable)
        }
    }

    fun stop() {
        try {
            if (serviceStarted) {
                nsdManager.unregisterService(registrationService)
                serviceStarted = false
            }
            if (discoveryService != null) {
                nsdManager.stopServiceDiscovery(discoveryService)
            }
        } catch (throwable: Throwable) {
            debug(TAG, throwable) // not ok when fails
        }
    }


    private data class DiscoveryService(
        val nsdManager: NsdManager,
        val consumer: (Peeraddr, String) -> Unit
    ) : DiscoveryListener {
        override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
            debug(TAG, "onStartDiscoveryFailed $serviceType")
        }

        override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
            debug(TAG, "onStopDiscoveryFailed $serviceType")
        }

        override fun onDiscoveryStarted(serviceType: String) {
            debug(TAG, "onDiscoveryStarted $serviceType")
        }

        override fun onDiscoveryStopped(serviceType: String) {
            debug(TAG, "onDiscoveryStopped $serviceType")
        }

        override fun onServiceFound(serviceInfo: NsdServiceInfo) {
            if (Build.VERSION.SDK_INT >= 34) {
                nsdManager.registerServiceInfoCallback(
                    serviceInfo, EXECUTOR,
                    object : ServiceInfoCallback {
                        override fun onServiceInfoCallbackRegistrationFailed(i: Int) {
                            debug(TAG, "onServiceInfoCallbackRegistrationFailed $i")
                        }

                        override fun onServiceUpdated(nsdServiceInfo: NsdServiceInfo) {
                            evaluateHosts(nsdServiceInfo)
                        }

                        override fun onServiceLost() {
                            debug(TAG, "onServiceLost")
                        }

                        override fun onServiceInfoCallbackUnregistered() {
                            debug(TAG, "onServiceInfoCallbackUnregistered")
                        }
                    })
            } else {
                nsdManager.resolveService(serviceInfo, object : NsdManager.ResolveListener {
                    override fun onResolveFailed(nsdServiceInfo: NsdServiceInfo, i: Int) {
                        debug(TAG, "onResolveFailed $nsdServiceInfo")
                    }

                    override fun onServiceResolved(nsdServiceInfo: NsdServiceInfo) {
                        evaluateHost(nsdServiceInfo)
                    }
                })
            }
        }

        override fun onServiceLost(serviceInfo: NsdServiceInfo) {
            debug(TAG, "onServiceLost $serviceInfo")
        }

        private fun evaluateHost(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = decodePeerId(serviceInfo.serviceName)

                val data = serviceInfo.attributes["name"]
                var name = ""
                if (data != null) {
                    name = String(data)
                }

                val inetAddress = serviceInfo.host

                val peeraddr =
                    createPeeraddr(peerId, inetAddress.address, serviceInfo.port.toUShort())

                consumer.invoke(peeraddr, name)
            } catch (throwable: Throwable) {
                debug(TAG, throwable) // is ok, fails for kubo
            }
        }

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private fun evaluateHosts(serviceInfo: NsdServiceInfo) {
            try {
                val peerId = decodePeerId(serviceInfo.serviceName)

                val peeraddrs = mutableListOf<Peeraddr>()
                for (inetAddress in serviceInfo.hostAddresses) {
                    peeraddrs.add(
                        createPeeraddr(peerId, inetAddress.address, serviceInfo.port.toUShort())
                    )
                }
                val data = serviceInfo.attributes["name"]
                var name = ""
                if (data != null) {
                    name = String(data)
                }
                if (!peeraddrs.isEmpty()) {
                    val peeraddr = bestReachablePeeraddr(peeraddrs)
                    if (peeraddr != null) {
                        consumer.invoke(peeraddr, name)
                    }
                }
            } catch (throwable: Throwable) {
                debug(TAG, throwable) // is ok, fails for kubo
            }
        }

        companion object {
            private val TAG: String = DiscoveryService::class.java.simpleName
        }
    }


    class RegistrationService : RegistrationListener {
        override fun onRegistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            debug(TAG, "onRegistrationFailed : $errorCode")
        }

        override fun onUnregistrationFailed(serviceInfo: NsdServiceInfo, errorCode: Int) {
            debug(TAG, "onUn-RegistrationFailed : $errorCode")
        }

        override fun onServiceRegistered(serviceInfo: NsdServiceInfo) {
            debug(TAG, "onServiceRegistered : " + serviceInfo.serviceName)
        }

        override fun onServiceUnregistered(serviceInfo: NsdServiceInfo) {
            debug(TAG, "onServiceUnregistered : " + serviceInfo.serviceName)
        }

        companion object {
            private val TAG: String = RegistrationService::class.java.simpleName
        }
    }

    companion object {
        private const val MDNS_SERVICE: String =
            "_p2p._udp." // default libp2p [or kubo] service name

        @RequiresApi(Build.VERSION_CODES.UPSIDE_DOWN_CAKE)
        private val EXECUTOR: ExecutorService = Executors.newSingleThreadExecutor()

        private val TAG: String = MDNS::class.java.name

        fun mdns(context: Context, mdnsName: String): MDNS {
            val nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
            return MDNS(nsdManager, mdnsName)
        }

        fun mdns(context: Context): MDNS {
            val nsdManager = context.getSystemService(Context.NSD_SERVICE) as NsdManager
            return MDNS(nsdManager, MDNS_SERVICE)
        }
    }
}


private fun removeLoopbacks(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isLoopbackAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

private fun removeLinkLocals(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isLinkLocalAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

private fun removeSiteLocals(peeraddrs: MutableList<Peeraddr>) {
    val iterator = peeraddrs.iterator()

    while (iterator.hasNext()) {
        val peeraddr = iterator.next()

        if (peeraddrs.size == 1) {
            return
        }
        try {
            val inetAddress = InetAddress.getByAddress(peeraddr.address)
            if (inetAddress.isSiteLocalAddress) {
                iterator.remove()
            }
        } catch (_: Throwable) {
            iterator.remove()
        }
    }
}

fun bestReachablePeeraddr(peeraddrs: MutableList<Peeraddr>): Peeraddr? {
    if (peeraddrs.size > 1) {
        removeLoopbacks(peeraddrs)
    }
    if (peeraddrs.size > 1) {
        removeLinkLocals(peeraddrs)
    }
    if (peeraddrs.size > 1) {
        removeSiteLocals(peeraddrs)
    }

    for (peer in peeraddrs) {
        val inetAddress = InetAddress.getByAddress(peer.address)
        if (inetAddress.isReachable(200)) { // only reachable
            return peer
        }
    }

    if (peeraddrs.isNotEmpty()) {
        return peeraddrs.first()
    }

    return null
}
