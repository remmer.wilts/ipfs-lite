package threads.odin.state

import android.app.Application
import android.graphics.Bitmap
import android.graphics.Color.BLACK
import android.graphics.Color.WHITE
import android.net.Uri
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.setValue
import androidx.core.net.toUri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.google.zxing.BarcodeFormat
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.createRequest
import tech.lp2p.odin.data.FileInfo
import threads.odin.App
import threads.odin.R
import threads.odin.model.API
import threads.odin.model.MDNS.Companion.mdns
import threads.odin.model.ODIN_PORT
import threads.odin.model.api
import threads.odin.model.createTempFile
import threads.odin.model.debug
import threads.odin.model.loopbackRequest
import threads.odin.model.reservationFeaturePossible
import threads.odin.work.InitPageWorker
import threads.odin.work.UploadFilesWorker
import java.io.PrintStream
import kotlin.concurrent.thread

class StateModel(private val application: Application) : AndroidViewModel(application) {

    private val mdns = mdns(application)

    var reachability: Int by mutableIntStateOf(R.string.unknown)

    val api: API = api(application)


    init {
        mdns.startService(api.dark(), api.title(), api.favicon())
    }

    fun numRelays(): Int {
        return api.numRelays
    }

    fun numConns(): Int {
        return api.numConns
    }

    fun fileInfos(): Flow<List<FileInfo>> {
        val app = application as App
        return app.files().filesDao().flowFileInfos()
    }

    fun reserveActive(): Boolean {
        return api.reserveActive
    }


    fun doHopReserve() {
        viewModelScope.launch {
            api.hopReserve()
        }
    }


    override fun onCleared() {
        debug("StateModel", "onCleared")
        super.onCleared()
        mdns.stop()
    }


    fun pageUri(): Uri {
        return createRequest(api.dark().peerId()).toUri()
    }

    fun localPageUri(): Uri {
        return loopbackRequest(api.dark().peerId(), ODIN_PORT).toUri()
    }

    fun delete(fileInfo: FileInfo) {
        val app = application as App
        viewModelScope.launch {
            // Note: the content itself (in the block store) will not be deleted
            // this is a limitation (idea the user has to cleanup the block store
            // from time to time
            app.files().filesDao().delete(fileInfo)
            InitPageWorker.initPage(getApplication())
        }

    }

    private fun networkReachability(reachability: Reachability): Int {
        return when (reachability) {
            Reachability.UNKNOWN -> R.string.unknown
            Reachability.LOCAL -> R.string.local_network
            Reachability.RELAYS -> R.string.relays_network
        }
    }


    fun evaluateReachability() {
        if (!reservationFeaturePossible()) {
            reachability(Reachability.LOCAL)
        } else {
            reachability(Reachability.RELAYS)
        }
    }

    fun reset() {
        val app = application as App
        thread {
            api.halo().reset()
            app.files().clearAllTables()

            InitPageWorker.initPage(getApplication())
        }
    }

    fun reachability(reachability: Reachability) {
        this.reachability = networkReachability(reachability)
    }

    fun loadUris(uris: List<Uri>) {
        viewModelScope.launch {

            if (uris.isNotEmpty()) {
                val file = createTempFile(getApplication())
                try {
                    PrintStream(file).use { out ->
                        for (uri in uris) {
                            out.println(uri.toString())
                        }
                    }
                } catch (throwable: Throwable) {
                    debug("StateModel", throwable)
                }
                UploadFilesWorker.load(getApplication(), file)
            }

        }
    }

    fun incomingConnections(): List<String> {
        return runBlocking { api.dark().incomingConnections() } // todo runBlocking
    }

    fun reservations(): List<Peeraddr> {
        return api.dark().reservations()
    }

    fun bitmap(): Bitmap {
        return getBitmap(pageUri().toString())
    }

    private fun getBitmap(content: String): Bitmap {
        val qrCodeWriter = QRCodeWriter()
        val bitMatrix = qrCodeWriter.encode(
            content, BarcodeFormat.QR_CODE,
            250, 250
        )
        return createBitmap(bitMatrix)
    }

    private fun createBitmap(matrix: BitMatrix): Bitmap {
        val width = matrix.width
        val height = matrix.height
        val pixels = IntArray(width * height)
        for (y in 0 until height) {
            val offset = y * width
            for (x in 0 until width) {
                pixels[offset + x] = if (matrix[x, y]) BLACK else WHITE
            }
        }

        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
        return bitmap
    }

    enum class Reachability {
        UNKNOWN, LOCAL, RELAYS
    }

}
