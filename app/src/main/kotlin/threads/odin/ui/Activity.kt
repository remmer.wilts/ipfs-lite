package threads.odin.ui

import android.content.Intent
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.ConnectivityManager.NetworkCallback
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Message
import android.view.View
import android.view.ViewGroup
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.widget.FrameLayout
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.safeDrawingPadding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.icons.outlined.Preview
import androidx.compose.material3.Badge
import androidx.compose.material3.BadgedBox
import androidx.compose.material3.BasicAlertDialog
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.BottomAppBarDefaults
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.FloatingActionButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxState
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.material3.minimumInteractiveComponentSize
import androidx.compose.material3.pulltorefresh.PullToRefreshBox
import androidx.compose.material3.pulltorefresh.rememberPullToRefreshState
import androidx.compose.material3.rememberModalBottomSheetState
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.graphics.createBitmap
import androidx.core.net.toUri
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.work.WorkManager
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.extractCid
import tech.lp2p.odin.Stream
import tech.lp2p.odin.data.FileInfo
import tech.lp2p.odin.model.MimeType
import threads.odin.R
import threads.odin.model.CONTENT_DOWNLOAD
import threads.odin.model.ODIN_PORT
import threads.odin.model.api
import threads.odin.model.compactString
import threads.odin.model.debug
import threads.odin.model.fileInfoSize
import threads.odin.model.getMediaResource
import threads.odin.model.host
import threads.odin.model.workUUID
import threads.odin.state.StateModel
import threads.odin.state.StateModel.Reachability

class Activity : ComponentActivity() {

    private val odin = "https://gitlab.com/lp2p/odin".toUri()

    private fun networkCallback(stateModel: StateModel) {

        val connectivityManager = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager


        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addTransportType(NetworkCapabilities.TRANSPORT_ETHERNET)
            .build()



        connectivityManager.registerNetworkCallback(networkRequest, object : NetworkCallback() {

            override fun onAvailable(network: Network) {
                stateModel.evaluateReachability()
            }

            override fun onCapabilitiesChanged(
                network: Network,
                networkCapabilities: NetworkCapabilities
            ) {
                stateModel.evaluateReachability()
            }

            override fun onLost(network: Network) {
                stateModel.reachability(Reachability.UNKNOWN)
            }
        })
    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun ResetDialog(stateModel: StateModel, onDismissRequest: () -> Unit) {
        BasicAlertDialog(onDismissRequest = { onDismissRequest.invoke() }) {
            Card(
                elevation = CardDefaults.cardElevation(
                    defaultElevation = 6.dp
                )
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                ) {
                    Text(
                        text = stringResource(id = R.string.warning),
                        style = MaterialTheme.typography.titleLarge,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.fillMaxWidth()
                    )
                    Text(
                        text = stringResource(id = R.string.reset_application_data),
                        style = MaterialTheme.typography.bodyMedium,
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp)
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(16.dp, 0.dp)
                    ) {
                        Button(onClick = { onDismissRequest.invoke() }) {
                            Text(
                                text = stringResource(id = android.R.string.cancel),
                            )
                        }
                        Box(modifier = Modifier.weight(1.0f, true))
                        Button(onClick = {
                            onDismissRequest.invoke()
                            stateModel.reset()
                        }
                        ) {
                            Text(
                                text = stringResource(id = android.R.string.ok),
                            )
                        }
                    }
                }
            }
        }
    }


    @Composable
    fun SwipeFileInfoItem(stateModel: StateModel, fileInfo: FileInfo) {

        var dismissState: SwipeToDismissBoxState? = null
        val threshold = .5f
        dismissState = rememberSwipeToDismissBoxState(
            confirmValueChange = { value ->
                if (value == SwipeToDismissBoxValue.EndToStart && dismissState!!.progress > threshold) {
                    stateModel.delete(fileInfo)
                    true
                } else {
                    false
                }
            },
            positionalThreshold = { it * threshold }
        )


        SwipeToDismissBox(
            modifier = Modifier.animateContentSize(),
            state = dismissState,
            backgroundContent = {

                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(MaterialTheme.colorScheme.errorContainer),
                    contentAlignment = Alignment.CenterEnd

                ) {
                    Icon(
                        Icons.Filled.Delete, stringResource(id = android.R.string.untitled),
                        modifier = Modifier.minimumInteractiveComponentSize()
                    )
                }
            },
            enableDismissFromEndToStart = true,
            enableDismissFromStartToEnd = false,
        ) {
            FileInfoItem(fileInfo)
        }

    }


    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun MainView(stateModel: StateModel) {

        var showResetDialog: Boolean by remember { mutableStateOf(false) }
        var showMenu: Boolean by remember { mutableStateOf(false) }
        var showRelays: Boolean by remember { mutableStateOf(false) }
        var showConns: Boolean by remember { mutableStateOf(false) }
        var showInfo: Boolean by remember { mutableStateOf(false) }
        var showHomepage: Boolean by remember { mutableStateOf(false) }

        val pickFiles = rememberLauncherForActivityResult(
            ActivityResultContracts.OpenMultipleDocuments()
        ) { uris -> stateModel.loadUris(uris) }


        var isRefreshing by remember { mutableStateOf(false) }
        val pullToRefreshState = rememberPullToRefreshState()
        val fileInfos by stateModel.fileInfos().collectAsState(emptyList())


        val snackbarHostState = remember { SnackbarHostState() }
        val scope = rememberCoroutineScope()
        val focusManager = LocalFocusManager.current

        val onRefresh: () -> Unit = {
            isRefreshing = true
            scope.launch {
                stateModel.doHopReserve()
                delay(1500)
                isRefreshing = false
            }
        }

        Scaffold(
            snackbarHost = {
                SnackbarHost(hostState = snackbarHostState)
            },
            topBar = {
                TopAppBar(
                    title = {
                        Text(
                            text = getString(R.string.app_name)
                        )
                    },
                    navigationIcon = {
                        IconButton(onClick = { showInfo = true }) {
                            Icon(
                                imageVector = Icons.Outlined.Home,
                                contentDescription = stringResource(id = R.string.home)
                            )
                        }
                    },
                    actions = {
                        IconButton(onClick = {
                            try {
                                val link = stateModel.pageUri().toString()

                                val intent = Intent(Intent.ACTION_SEND)
                                intent.putExtra(
                                    Intent.EXTRA_SUBJECT, getString(R.string.share_link)
                                )
                                intent.putExtra(Intent.EXTRA_TEXT, link)
                                intent.setType(MimeType.PLAIN_MIME_TYPE.name)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

                                val chooser = Intent.createChooser(intent, getText(R.string.share))
                                chooser.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                startActivity(chooser)
                            } catch (_: Throwable) {
                                scope.launch {
                                    snackbarHostState.showSnackbar(
                                        message = getString(R.string.no_activity_found_to_handle_uri),
                                        duration = SnackbarDuration.Short
                                    )
                                }
                            }
                        }) {
                            Icon(
                                imageVector = Icons.Default.Share,
                                contentDescription = stringResource(id = R.string.share)
                            )
                        }
                        IconButton(onClick = {
                            showMenu = true
                        }) {
                            Icon(
                                imageVector = Icons.Outlined.MoreVert,
                                contentDescription = stringResource(id = android.R.string.untitled)
                            )
                        }
                        DropdownMenu(
                            expanded = showMenu,
                            onDismissRequest = { showMenu = false })
                        {
                            DropdownMenuItem(
                                text = { Text(text = stringResource(id = R.string.documentation)) },

                                onClick = {
                                    try {
                                        val intent = Intent(Intent.ACTION_VIEW)
                                        intent.setData(odin)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                        startActivity(intent)
                                    } catch (_: Throwable) {
                                        scope.launch {
                                            snackbarHostState.showSnackbar(
                                                message = getString(R.string.no_activity_found_to_handle_uri),
                                                withDismissAction = true,
                                                duration = SnackbarDuration.Indefinite
                                            )
                                        }
                                    } finally {
                                        showMenu = false
                                    }
                                })

                            DropdownMenuItem(
                                text = { Text(text = stringResource(id = R.string.reset)) },

                                onClick = {
                                    showResetDialog = true
                                    showMenu = false
                                })
                        }
                    },
                    scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior()
                )
            },
            bottomBar = {
                BottomAppBar(
                    actions = {

                        BadgedBox(
                            badge = {
                                Badge {
                                    Text(text = stateModel.numRelays().toString())
                                }
                            },
                            modifier = Modifier.padding(16.dp, 0.dp, 16.dp, 0.dp)
                        ) {
                            IconButton(onClick = { showRelays = true }) {
                                Icon(
                                    painterResource(id = R.drawable.access_point_network),
                                    contentDescription = stringResource(id = R.string.relays)
                                )
                            }
                        }

                        BadgedBox(
                            badge = {
                                Badge {
                                    Text(text = stateModel.numConns().toString())
                                }
                            },
                            modifier = Modifier.padding(16.dp, 0.dp, 16.dp, 0.dp)
                        ) {
                            IconButton(onClick = { showConns = true }) {
                                Icon(
                                    painterResource(id = R.drawable.lan_connect),
                                    contentDescription = stringResource(id = R.string.connections)
                                )
                            }
                        }

                        IconButton(onClick = { showHomepage = true }) {
                            Icon(
                                imageVector = Icons.Outlined.Preview,
                                contentDescription = stringResource(id = R.string.homepage)
                            )
                        }
                    },
                    floatingActionButton = {
                        FloatingActionButton(
                            onClick = {
                                pickFiles.launch(arrayOf(MimeType.ALL.name))
                            },
                            containerColor = BottomAppBarDefaults.bottomAppBarFabColor,
                            elevation = FloatingActionButtonDefaults.bottomAppBarFabElevation()
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.plus_thick),
                                contentDescription = stringResource(id = R.string.files)
                            )
                        }
                    }
                )
            },
        ) { innerPadding ->

            val listState = rememberLazyListState()
            Column(
                modifier = Modifier
                    .padding(innerPadding)
                    .fillMaxSize()
                    .pointerInput(Unit) {
                        detectTapGestures(onTap = {
                            focusManager.clearFocus()
                        })
                    }
            ) {

                Text(
                    text = stringResource(id = stateModel.reachability),
                    style = MaterialTheme.typography.labelLarge,
                    color = if (stateModel.reserveActive()) {
                        MaterialTheme.colorScheme.error
                    } else {
                        MaterialTheme.colorScheme.onSurface
                    },
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.CenterHorizontally)
                        .background(
                            if (stateModel.reserveActive()) {
                                MaterialTheme.colorScheme.onError
                            } else {
                                MaterialTheme.colorScheme.surface
                            }
                        )
                )

                PullToRefreshBox(
                    isRefreshing = isRefreshing,
                    state = pullToRefreshState,
                    onRefresh = onRefresh
                ) {
                    LazyColumn(
                        modifier = Modifier.fillMaxSize(),
                        state = listState
                    ) {
                        items(items = fileInfos, key = { fileItem ->
                            fileItem.idx
                        }) { fileInfo ->
                            SwipeFileInfoItem(stateModel, fileInfo)
                        }
                    }
                }

                if (isRefreshing) {
                    LaunchedEffect(Unit) {
                        stateModel.doHopReserve()
                        delay(1000)
                        isRefreshing = false
                    }
                }
            }
        }


        if (showRelays) {
            ModalBottomSheet(onDismissRequest = { showRelays = false }) {
                RelaysView(stateModel.reservations())
            }
        }

        if (showConns) {
            ModalBottomSheet(onDismissRequest = { showConns = false }) {
                ConnsView(stateModel.incomingConnections())
            }
        }

        if (showInfo) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                sheetState = sheetState,
                onDismissRequest = { showInfo = false }) {
                InfoView(stateModel)
            }
        }

        if (showHomepage) {
            val sheetState = rememberModalBottomSheetState(
                skipPartiallyExpanded = true
            )
            ModalBottomSheet(
                containerColor = Color.Black.copy(0.3f),
                sheetState = sheetState,
                modifier = Modifier
                    .fillMaxHeight()
                    .safeDrawingPadding(),
                onDismissRequest = { showHomepage = false }) {
                Homepage(stateModel)
            }
        }

        if (showResetDialog) {
            ResetDialog(
                stateModel = stateModel,
                onDismissRequest = {
                    showResetDialog = false
                })
        }
    }

    @Composable
    fun InfoView(stateModel: StateModel) {

        val bitmap by remember { mutableStateOf(stateModel.bitmap()) }
        val homepage by remember { mutableStateOf(stateModel.pageUri().toString()) }

        Column(
            modifier = Modifier
                .padding(0.dp, 0.dp, 0.dp, 32.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Text(
                text = stringResource(id = R.string.information),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth()
            )

            Text(
                text = stringResource(id = R.string.limitation),
                style = MaterialTheme.typography.labelLarge,
                modifier = Modifier.padding(16.dp)
            )

            Image(
                bitmap = bitmap.asImageBitmap(),
                contentDescription = stringResource(id = R.string.uri_qrcode),
                modifier = Modifier
                    .size(240.dp)
                    .fillMaxWidth()
                    .padding(16.dp)
                    .align(Alignment.CenterHorizontally)
            )

            Row(modifier = Modifier.padding(16.dp)) {
                Icon(
                    painterResource(id = R.drawable.view),
                    contentDescription = stringResource(id = android.R.string.untitled)
                )
                Text(
                    text = homepage,
                    style = MaterialTheme.typography.labelMedium,
                    softWrap = false,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(1.0f, true)
                        .align(Alignment.CenterVertically)
                        .offset(8.dp)
                )
            }

            Spacer(modifier = Modifier.padding(32.dp))


            Text(
                text = stringResource(R.string.port_forwarding),
                style = MaterialTheme.typography.titleMedium,
                softWrap = false,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )


            Text(
                text = stringResource(id = R.string.port_forwarding_info),
                style = MaterialTheme.typography.labelLarge,
                modifier = Modifier.padding(16.dp)
            )

            Row(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = stringResource(R.string.ipv),
                    style = MaterialTheme.typography.labelLarge,
                    softWrap = false,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(1.0f, true)
                        .align(Alignment.CenterVertically)
                        .padding(32.dp, 0.dp, 0.dp, 0.dp)
                )
                Text(
                    text = "IPv6",
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary,
                    softWrap = false,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(0.dp, 0.dp, 32.dp, 0.dp)
                )
            }

            Row(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = stringResource(R.string.port),
                    style = MaterialTheme.typography.labelLarge,
                    softWrap = false,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(1.0f, true)
                        .align(Alignment.CenterVertically)
                        .padding(32.dp, 0.dp, 0.dp, 0.dp)
                )
                Text(
                    text = ODIN_PORT.toString(),
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary,
                    softWrap = false,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(0.dp, 0.dp, 32.dp, 0.dp)
                )
            }

            Row(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = stringResource(R.string.protocol),
                    style = MaterialTheme.typography.labelLarge,
                    softWrap = false,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(1.0f, true)
                        .align(Alignment.CenterVertically)
                        .padding(32.dp, 0.dp, 0.dp, 0.dp)
                )
                Text(
                    text = "TCP",
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary,
                    softWrap = false,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(0.dp, 0.dp, 32.dp, 0.dp)
                )
            }
            Row(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = stringResource(R.string.application),
                    style = MaterialTheme.typography.labelLarge,
                    softWrap = false,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .weight(1.0f, true)
                        .align(Alignment.CenterVertically)
                        .padding(32.dp, 0.dp, 0.dp, 0.dp)
                )
                Text(
                    text = stringResource(R.string.app_name),
                    style = MaterialTheme.typography.labelLarge,
                    color = MaterialTheme.colorScheme.primary,
                    softWrap = false,
                    textAlign = TextAlign.End,
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(0.dp, 0.dp, 32.dp, 0.dp)
                )
            }
        }

        Spacer(modifier = Modifier.padding(32.dp))

    }


    @Composable
    fun ConnsView(addresses: List<String>) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 0.dp, 0.dp, 32.dp)
        ) {
            Text(
                text = stringResource(id = R.string.connections),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth()
            )


            LazyColumn {
                items(items = addresses) { item ->
                    AddressItem(item)
                }
            }
        }
    }

    @Composable
    fun FileInfoItem(fileInfo: FileInfo) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.surface)

        ) {
            Row(modifier = Modifier.padding(8.dp)) {
                val uuid = workUUID(fileInfo)

                Box(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .minimumInteractiveComponentSize()
                ) {
                    Icon(
                        painterResource(id = getMediaResource(fileInfo.mimeType)),
                        contentDescription = stringResource(id = android.R.string.untitled),
                        modifier = Modifier
                            .size(24.dp)
                            .align(Alignment.Center)
                    )
                    if (uuid != null) {
                        CircularProgressIndicator(
                            strokeWidth = 2.dp,
                            modifier = Modifier.align(Alignment.Center)
                        )
                    }
                }

                Column(
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .weight(1.0f, true)
                ) {
                    Text(
                        text = compactString(fileInfo.name), maxLines = 1,
                        style = MaterialTheme.typography.bodyMedium, softWrap = true
                    )
                    Text(
                        text = fileInfoSize(fileInfo), maxLines = 1,
                        style = MaterialTheme.typography.bodySmall, softWrap = true
                    )
                }

                if (uuid != null) {
                    val context = LocalContext.current
                    IconButton(
                        modifier = Modifier.align(Alignment.CenterVertically),
                        onClick = {
                            WorkManager
                                .getInstance(context)
                                .cancelWorkById(uuid)
                        })
                    {
                        Icon(
                            Icons.Default.Close,
                            contentDescription = stringResource(
                                id =
                                    android.R.string.cancel
                            ),
                        )
                    }
                }
            }
        }
    }

    @Composable
    fun AddressItem(address: String, modifier: Modifier = Modifier) {
        Column(
            modifier = modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.surface)
        ) {
            Row(
                modifier = modifier
                    .padding(16.dp)
                    .fillMaxWidth()
            ) {
                Icon(
                    painterResource(id = R.drawable.server_network),
                    contentDescription = stringResource(id = android.R.string.untitled),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )

                Text(
                    modifier = Modifier
                        .padding(16.dp, 0.dp, 0.dp, 0.dp)
                        .weight(1.0f, true)
                        .fillMaxWidth()
                        .align(Alignment.CenterVertically),
                    text = address,
                    maxLines = 1,
                    style = MaterialTheme.typography.labelSmall, softWrap = false
                )

            }
        }
    }

    @Composable
    fun PeeraddrItem(peeraddr: Peeraddr, modifier: Modifier = Modifier) {
        Column(
            modifier = modifier
                .fillMaxWidth()
                .background(MaterialTheme.colorScheme.surface)
        ) {
            Row(
                modifier = modifier
                    .padding(16.dp)
                    .fillMaxWidth()
            ) {
                Icon(
                    painterResource(id = R.drawable.server_network),
                    contentDescription = stringResource(id = android.R.string.untitled),
                    modifier = Modifier
                        .size(24.dp)
                        .align(
                            Alignment.CenterVertically
                        )
                )

                Column(
                    modifier = Modifier
                        .padding(16.dp, 0.dp, 0.dp, 0.dp)
                        .weight(1.0f, true)
                ) {
                    Text(
                        text = peeraddr.peerId.toBase58(), maxLines = 1,
                        modifier = Modifier.fillMaxWidth(),
                        style = MaterialTheme.typography.bodySmall, softWrap = false
                    )
                    Text(
                        text = host(peeraddr) + ":" + peeraddr.port, maxLines = 2,
                        modifier = Modifier.fillMaxWidth(),
                        style = MaterialTheme.typography.labelSmall, softWrap = false
                    )
                }
            }
        }
    }


    @Composable
    fun KeepScreenOn() {
        val currentView = LocalView.current
        DisposableEffect(Unit) {
            currentView.keepScreenOn = true
            onDispose {
                currentView.keepScreenOn = false
            }
        }
    }

    @Composable
    fun AppTheme(useDarkTheme: Boolean = isSystemInDarkTheme(), content: @Composable () -> Unit) {
        val context = LocalContext.current

        val lightingColorScheme = lightColorScheme()

        val colorScheme =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                if (useDarkTheme)
                    dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
            } else {
                lightingColorScheme
            }

        MaterialTheme(
            colorScheme = colorScheme,
            content = content
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        enableEdgeToEdge()

        setContent {
            val stateModel: StateModel = viewModel()
            networkCallback(stateModel)
            KeepScreenOn()
            AppTheme {
                Box(modifier = Modifier.safeDrawingPadding()) {
                    MainView(stateModel)
                }
            }
        }
    }


    @Composable
    fun RelaysView(list: List<Peeraddr>) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 0.dp, 0.dp, 32.dp)
        ) {
            Text(
                text = stringResource(id = R.string.relays),
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center, modifier = Modifier.fillMaxWidth()
            )

            LazyColumn {
                items(items = list) { item ->
                    PeeraddrItem(item)
                }
            }
        }
    }


    @Composable
    fun Homepage(stateModel: StateModel) {
        val webViewNavigator = rememberWebViewNavigator()
        val webViewState = rememberSaveableWebViewState()

        Scaffold(
            floatingActionButton = {
                if (webViewNavigator.canGoBack) {
                    FloatingActionButton(
                        containerColor = MaterialTheme.colorScheme.primary,
                        contentColor = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier.padding(8.dp),
                        onClick = {
                            webViewNavigator.navigateBack()
                        }) {
                        Icon(
                            Icons.AutoMirrored.Filled.ArrowBack,
                            contentDescription = stringResource(android.R.string.untitled)
                        )
                    }
                }
            },
            content = { padding ->
                WebView(
                    modifier = Modifier
                        .padding(padding)
                        .fillMaxSize(),
                    state = webViewState,
                    navigator = webViewNavigator,
                    chromeClient = remember {
                        CustomWebChromeClient()
                    },
                    client = remember {
                        CustomWebViewClient()
                    },
                    onInitPage = {
                        stateModel.localPageUri().toString()
                    },
                    onCreated = { view ->
                        CookieManager.getInstance().setAcceptThirdPartyCookies(view, false)

                        view.setDownloadListener { downloadUrl: String,
                                                   userAgent: String?,
                                                   contentDisposition: String?,
                                                   mimeType: String,
                                                   contentLength: Long ->
                            try {
                                val uri = downloadUrl.toUri()

                                debug("Activity", uri.toString())
                                debug("Activity", "UserAgent $userAgent")
                                debug("Activity", "ContentDisposition $contentDisposition")
                                debug("Activity", "MimeType $mimeType")

                            } catch (throwable: Throwable) {
                                debug("Activity", throwable)
                            }
                        }
                    })
            })

    }


    private inner class CustomWebViewClient() : AccompanistWebViewClient() {
        private var halo = api(application).halo()

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {

            val uri = request.url
            debug("Activity", "shouldOverrideUrlLoading : $uri")

            when (uri.scheme) {
                "pns" -> {
                    val name = uri.getQueryParameter(CONTENT_DOWNLOAD)
                    if (name != null) {
                        Toast.makeText(
                            application,
                            application.getString(R.string.no_activity_found_to_handle_uri),
                            Toast.LENGTH_SHORT
                        ).show()
                        return true
                    }
                }
            }

            return false
        }

        override fun shouldInterceptRequest(
            view: WebView,
            request: WebResourceRequest
        ): WebResourceResponse? {
            val uri = request.url

            debug("Activity", "shouldInterceptRequest : $uri")

            if (uri.scheme == "pns") {
                val cid = extractCid(uri.toString())
                val response = runBlocking { halo.response(cid) }
                return WebResourceResponse(
                    response.mimeType, response.encoding, response.status,
                    response.reason, response.headers, Stream(response.channel)
                )
            }
            return null
        }
    }


    private inner class CustomWebChromeClient() : AccompanistWebChromeClient() {
        private var mCustomView: View? = null
        private var mCustomViewCallback: CustomViewCallback? = null


        override fun onHideCustomView() {
            super.onHideCustomView()


            (this@Activity.window.decorView as FrameLayout).removeView(this.mCustomView)
            this.mCustomView = null

            mCustomViewCallback!!.onCustomViewHidden()
            this.mCustomViewCallback = null
        }

        override fun onCreateWindow(
            view: WebView,
            dialog: Boolean,
            userGesture: Boolean,
            resultMsg: Message
        ): Boolean {
            val result = view.hitTestResult
            val data = result.extra?.toUri()
            val context = view.context
            val browserIntent =
                Intent(
                    Intent.ACTION_VIEW,
                    data,
                    context,
                    android.app.Activity::class.java
                )
            context.startActivity(browserIntent)
            return false
        }

        override fun onShowCustomView(
            paramView: View,
            paramCustomViewCallback: CustomViewCallback
        ) {
            super.onShowCustomView(paramView, paramCustomViewCallback)


            this.mCustomView = paramView
            this.mCustomViewCallback = paramCustomViewCallback
            (this@Activity.window.decorView as FrameLayout)
                .addView(
                    this.mCustomView, FrameLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
                    )
                )

        }

        override fun getDefaultVideoPoster(): Bitmap {
            return createBitmap(10, 10)
        }
    }
}


