package threads.odin.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import threads.odin.App

class DeleteFileWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {
        val idx = inputData.getLong(IDX, -1)
        val app = applicationContext as App
        val files = app.files()
        files.delete(idx)
        return Result.success()
    }


    companion object {
        private val TAG: String = DeleteFileWorker::class.java.simpleName
        private const val IDX = "idx"

        private fun getWork(idx: Long): OneTimeWorkRequest {
            val data: Data.Builder = Data.Builder()
            data.putLong(IDX, idx)

            return OneTimeWorkRequestBuilder<DeleteFileWorker>()
                .addTag(TAG)
                .setInputData(data.build())
                .build()
        }

        fun delete(context: Context, idx: Long) {
            WorkManager.getInstance(context).enqueue(getWork(idx))
        }
    }
}
