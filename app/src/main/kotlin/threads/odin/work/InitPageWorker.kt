package threads.odin.work

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import threads.odin.App
import threads.odin.model.api
import threads.odin.model.debug
import threads.odin.model.directoryContent

class InitPageWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {
        try {
            val app = applicationContext as App
            val api = api(applicationContext)
            val fileInfos = app.files().filesDao().fileInfos()
            val content: String = directoryContent(fileInfos, api.title(), api.favicon())
            api.halo().root(content.toByteArray())
        } catch (throwable: Throwable) {
            debug(TAG, throwable)
        }
        return Result.success()
    }


    companion object {
        private val TAG: String = InitPageWorker::class.java.simpleName

        private fun getWork(): OneTimeWorkRequest {

            return OneTimeWorkRequestBuilder<InitPageWorker>()
                .addTag(TAG)
                .build()
        }

        fun initPage(context: Context) {
            WorkManager.getInstance(context).enqueue(getWork())
        }
    }
}
