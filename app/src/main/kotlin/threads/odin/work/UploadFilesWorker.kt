package threads.odin.work

import android.content.Context
import androidx.core.net.toUri
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runInterruptible
import kotlinx.coroutines.withContext
import kotlinx.io.Buffer
import kotlinx.io.RawSource
import kotlinx.io.buffered
import tech.lp2p.halo.Node
import tech.lp2p.odin.data.FileInfo
import threads.odin.App
import threads.odin.model.api
import threads.odin.model.checkMimeType
import threads.odin.model.debug
import threads.odin.model.fileName
import threads.odin.model.fileSize
import threads.odin.model.getUniqueName
import threads.odin.model.mimeType
import threads.odin.model.tempFile
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

class UploadFilesWorker(context: Context, params: WorkerParameters) :
    CoroutineWorker(context, params) {


    override suspend fun doWork(): Result {
        val filename = inputData.getString(FILE)
        checkNotNull(filename)
        val app = applicationContext as App
        val files = app.files()
        val halo = api(applicationContext).halo()
        val file = tempFile(applicationContext, filename)

        val uris: MutableList<String> = ArrayList()
        withContext(Dispatchers.IO) {
            try {
                BufferedReader(
                    InputStreamReader(FileInputStream(file))
                ).use { reader ->
                    while (reader.ready()) {
                        uris.add(reader.readLine())
                    }
                }

                val names = files.fileNames().toMutableList()

                for (uriStr in uris) {
                    val uri = uriStr.toUri()

                    val displayName = fileName(applicationContext, uri)
                    val uriType = mimeType(applicationContext, uri)

                    val name = getUniqueName(names, displayName)
                    val mimeType = checkMimeType(uriType, displayName)

                    val size = fileSize(applicationContext, uri)


                    val fileInfo = FileInfo(
                        0, name, mimeType!!,
                        0, id.toString(), size
                    )
                    val idx = files.storeFileInfo(fileInfo)

                    try {
                        var node: Node? = null
                        runInterruptible {
                            applicationContext.contentResolver
                                .openInputStream(uri).use { inputStream ->
                                    checkNotNull(inputStream)
                                    val bytes = ByteArray(Short.MAX_VALUE.toInt())
                                    object : RawSource {

                                        override fun readAtMostTo(
                                            sink: Buffer,
                                            byteCount: Long
                                        ): Long {
                                            try {
                                                // TODO maybe it can go faster
                                                val read = inputStream.read(bytes)
                                                if (read >= 0) {
                                                    sink.write(bytes, 0, read)
                                                }
                                                return read.toLong()
                                            } catch (throwable: Throwable) {
                                                debug("UploadFilesWorker", throwable)
                                            }
                                            return -1
                                        }

                                        override fun close() {
                                            inputStream.close()
                                        }

                                    }.buffered().use { source ->
                                        node = halo.storeSource(source, name, mimeType)
                                    }

                                }
                        }
                        files.done(idx, node!!.cid())
                    } catch (_: Throwable) {
                        DeleteFileWorker.delete(applicationContext, idx)
                        break
                    } finally {
                        names.add(name) // just for safety
                    }
                }
            } catch (throwable: Throwable) {
                debug(TAG, throwable)
            } finally {
                file.deleteOnExit()
                InitPageWorker.initPage(applicationContext)
            }
        }

        return Result.success()
    }


    companion object {
        private val TAG: String = UploadFilesWorker::class.java.simpleName
        private const val FILE = "file"

        private fun getWork(file: File): OneTimeWorkRequest {
            val data: Data.Builder = Data.Builder()
            data.putString(FILE, file.name)

            return OneTimeWorkRequestBuilder<UploadFilesWorker>()
                .addTag(TAG)
                .setInputData(data.build())
                .build()
        }

        fun load(context: Context, file: File) {
            WorkManager.getInstance(context).enqueue(getWork(file))
        }
    }
}
