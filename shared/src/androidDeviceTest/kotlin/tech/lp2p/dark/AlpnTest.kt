package tech.lp2p.dark

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.core.Raw
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class AlpnTest {
    @Test
    fun alpnTest(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()

        val server = newDark()
        server.runService(halo, serverPort)

        val input = TestEnv.getRandomBytes(100) //

        halo.root(input)
        val cid = halo.root().cid()

        val cmp = (halo.root() as Raw).data()
        assertTrue(input.contentEquals(cmp))


        val peeraddrs = TestEnv.peeraddrs(server.peerId(), serverPort)
        val peeraddr = peeraddrs.first()
        val client = newDark()

        val requestRoot = createRequest(peeraddr)
        val rootUri = client.fetchRoot(requestRoot)
        assertNotNull(rootUri)

        val root = extractCid(rootUri)
        checkNotNull(root)

        assertEquals(root, halo.root().cid())

        val serverAddress = extractPeeraddr(rootUri)
        assertEquals(serverAddress, peeraddr)

        val request = createRequest(peeraddr, cid)

        val data = client.fetchData(request)
        assertTrue(input.contentEquals(data))
        client.shutdown()

        server.shutdown()
        halo.delete()
    }

}