package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class ChannelStressTest {
    @Test
    fun channelTestRun(): Unit = runBlocking {
        // create server instance with default values
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()

        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)
        server.runService(halo, serverPort)
        val client = newDark()// client instance default values
        repeat(100) {

            val node =
                halo.storeData(
                    TestEnv.getRandomBytes(SPLITTER_SIZE.toInt())
                ) // store some text
            val request = createRequest(loopback, node)

            val data = client.fetchData(request) // fetch request
            assertNotNull(data)
            assertTrue(halo.fetchData(node).contentEquals(data))
        }
        client.shutdown()
        server.shutdown()
        halo.delete()
    }
}