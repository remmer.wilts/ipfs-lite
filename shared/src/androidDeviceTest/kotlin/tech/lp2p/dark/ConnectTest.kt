package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class ConnectTest {


    @Test
    fun testServerIdle(): Unit = runBlocking {
        if (!TestEnv.supportLongRunningTests()) {
            return@runBlocking
        }
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val request = TestEnv.loopbackRequest(server.peerId(), serverPort)
        val client = newDark()
        val root = client.fetchRoot(request)
        assertNotNull(root)

        Thread.sleep(10000) // timeout is 10 sec (should be reached)
        assertEquals(server.numIncomingConnections(), 1)
        // but connection is still valid (keep alive is true)
        client.shutdown()
        server.shutdown()
        halo.delete()
    }

    @Test
    fun testClientClose(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val request = TestEnv.loopbackRequest(server.peerId(), serverPort)
        val client = newDark()
        val rootUri = client.fetchRoot(request)
        assertNotNull(rootUri)

        val data = client.fetchData(rootUri)
        assertNotNull(data)
        assertTrue(data.contentEquals(byteArrayOf()))

        assertEquals(server.numIncomingConnections(), 1)


        assertEquals(server.numOutgoingConnections(), 0)
        assertEquals(server.numIncomingConnections(), 1)

        assertEquals(client.numOutgoingConnections(), 1)
        assertEquals(client.numIncomingConnections(), 0)
        client.shutdown()

        server.shutdown()
        halo.delete()
    }

    @Test
    fun testServerClose(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val request = TestEnv.loopbackRequest(server.peerId(), serverPort)
        val client = newDark()
        val rootUri = client.fetchRoot(request) // Intern it sets keep alive to true
        assertNotNull(rootUri)

        val data = client.fetchData(rootUri)
        assertNotNull(data)
        assertTrue(data.contentEquals(byteArrayOf()))


        assertEquals(server.numOutgoingConnections(), 0)
        assertEquals(server.numIncomingConnections(), 1)

        assertEquals(client.numOutgoingConnections(), 1)
        assertEquals(client.numIncomingConnections(), 0)


        assertEquals(server.numReservations(), 0)
        val peeraddrs = server.incomingConnections()
        assertFalse(peeraddrs.isEmpty())

        client.shutdown()

        server.shutdown()
        halo.delete()
    }
}
