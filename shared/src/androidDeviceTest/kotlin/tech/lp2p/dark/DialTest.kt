package tech.lp2p.dark

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class DialTest {


    @Test
    fun testDial(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()

        if (!TestEnv.reservationFeaturePossible()) {
            println("reservation feature not available")
            return@runBlocking
        }
        val halo = newHalo()
        val server = newDark(bootstrap = TestEnv.BOOTSTRAP)
        server.runService(halo, serverPort)

        val peerStore = server.peerStore()

        halo.root("Homepage".toByteArray())
        val raw = halo.root().cid()

        var publicPeeraddrs = TestEnv.peeraddrs(server.peerId(), serverPort)

        server.makeReservations(publicPeeraddrs, 25, 120)

        assertTrue(server.hasReservations())

        val client = Dark(
            peerStore = peerStore,
            bootstrap = TestEnv.BOOTSTRAP
        )
        val uri = createRequest(server.peerId())
        assertNotNull(uri)

        val rootUri = client.fetchRoot(uri)
        assertNotNull(rootUri)
        val cid = extractCid(rootUri)
        assertEquals(cid, raw)
        client.shutdown()


        server.shutdown()
        halo.delete()
    }

    @Test
    fun testDialAll(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()

        if (!TestEnv.reservationFeaturePossible()) {
            println("reservation feature not available")
            return@runBlocking
        }
        val halo = newHalo()
        val server = newDark(bootstrap = TestEnv.BOOTSTRAP)
        server.runService(halo, serverPort)

        var publicPeeraddrs = TestEnv.peeraddrs(server.peerId(), serverPort)

        server.makeReservations(publicPeeraddrs, 25, 120)

        val reservations = server.reservations()

        val success = atomic(0)

        for (relay in reservations) {
            val client = newDark()
            try {

                val peeraddrs = client.findPeer(relay, server.peerId())
                assertEquals(publicPeeraddrs, peeraddrs)
                success.incrementAndGet()

            } catch (throwable: Throwable) {
                throwable.printStackTrace()
            } finally {
                client.shutdown()
            }
        }
        assertTrue(server.hasReservations())
        println("Success " + success.value + " Total " + reservations.size)

        assertTrue(success.value > 0)

        server.shutdown()
        halo.delete()
    }
}