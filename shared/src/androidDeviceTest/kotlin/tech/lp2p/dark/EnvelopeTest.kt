package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import tech.lp2p.asen.Keys
import tech.lp2p.asen.PeerId
import tech.lp2p.halo.newHalo
import tech.lp2p.lite.sign
import tech.lp2p.lite.verify
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.ObjectInputStream
import java.io.ObjectOutputStream
import java.io.Serializable
import kotlin.test.Test
import kotlin.test.assertNotNull


class EnvelopeTest {
    @Test
    fun envelopeBasic(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        // create server and client instance
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val client = newDark()

        val text = halo.storeText("moin")
        val envelope = createEnvelope(
            server.keys(),
            Payload(20, System.currentTimeMillis(), text.cid())
        )
        assertNotNull(envelope)

        val transformed = toArray(envelope)
        assertNotNull(transformed)

        val raw = halo.storeData(transformed) // store some text

        val request =
            createRequest(TestEnv.loopbackPeeraddr(server.peerId(), serverPort), raw)

        val data = client.fetchData(request) // fetch request

        verifyEnvelope(server.peerId(), data)
        client.shutdown()
        server.shutdown()
        halo.delete()
    }
}


data class Envelope(val payload: Payload, val signature: ByteArray) :
    Serializable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val envelope = other as Envelope
        return payload == envelope.payload &&
                signature.contentEquals(envelope.signature)
    }

    override fun hashCode(): Int {
        var result = payload.hashCode()
        result = 31 * result + signature.contentHashCode()
        return result
    }


}


data class Payload(@JvmField val type: Int, val timestamp: Long, @JvmField val cid: Long) :
    Serializable

private fun toEnvelope(data: ByteArray): Envelope {
    checkNotNull(data) { "data can not be null" }

    try {
        ByteArrayInputStream(data).use { inputStream ->
            ObjectInputStream(inputStream).use { objectInputStream ->
                return objectInputStream.readObject() as Envelope
            }
        }
    } catch (throwable: Throwable) {
        throw IllegalStateException(throwable)
    }
}

private fun writeUnsignedVariant(out: Buffer, value: Long) {
    var value = value
    var remaining = value ushr 7
    while (remaining != 0L) {
        out.writeByte(((value and 0x7fL) or 0x80L).toByte())
        value = remaining
        remaining = remaining ushr 7
    }
    out.writeByte((value and 0x7fL).toByte())
}

fun unsignedEnvelopePayload(payload: Payload): ByteArray {
    val data = Buffer()
    data.writeLong(payload.cid)

    val buffer = Buffer()

    writeUnsignedVariant(buffer, payload.type.toLong())
    writeUnsignedVariant(buffer, payload.timestamp)
    writeUnsignedVariant(buffer, data.size.toLong())

    buffer.write(data.readByteArray())
    return buffer.readByteArray()
}


fun createEnvelope(keys: Keys, type: Payload): Envelope {
    val payload = unsignedEnvelopePayload(type)
    val signature = sign(keys, payload)
    return Envelope(type, signature)
}


fun toArray(envelope: Envelope): ByteArray {
    checkNotNull(envelope) { "envelope can not be null" }

    try {
        ByteArrayOutputStream().use { outputStream ->
            ObjectOutputStream(outputStream).use { objectOutputStream ->
                objectOutputStream.writeObject(envelope)
                return outputStream.toByteArray()
            }
        }
    } catch (throwable: Throwable) {
        throw IllegalStateException(throwable)
    }
}

fun verifyEnvelope(peerId: PeerId, data: ByteArray) {
    val envelope = toEnvelope(data)
    val payload = envelope.payload
    val signature = envelope.signature
    verify(peerId, unsignedEnvelopePayload(payload), signature)
}