package tech.lp2p.dark

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FetchCidStressTest {
    @Test
    fun fetchCidIterations(): Unit = runBlocking(Dispatchers.IO) {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        halo.root("Homepage".toByteArray())
        val raw = halo.root().cid()

        val request = TestEnv.loopbackRequest(server.peerId(), serverPort)
        val client = newDark()
        repeat(TestEnv.ITERATIONS) {
            val rootUri = client.fetchRoot(request)
            assertNotNull(rootUri)
            val value = extractCid(rootUri)
            assertEquals(value, raw)
        }
        client.shutdown()

        server.shutdown()
        halo.delete()
    }
}