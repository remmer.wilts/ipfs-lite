package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class FetchServerTest {
    @Test
    fun fetchDataTest(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)

        var timestamp = System.currentTimeMillis()

        val fid = TestEnv.createContent(halo, 100) // 100 * SPLITTER_SIZE
        assertNotNull(fid)

        println(
            ("Time Create Content " + (System.currentTimeMillis() - timestamp)
                    + " [ms] Size " + (fid.size() / 1000000) + " MB")
        )



        timestamp = System.currentTimeMillis()
        val client = newDark()
        val request = createRequest(loopback, fid)
        val out = halo.tempFile()
        val channel = client.channel(request)


        SystemFileSystem.sink(out).buffered().use { sink ->
            channel.transferTo(sink) {}
        }

        println(
            "Time Client Read Content " +
                    (System.currentTimeMillis() - timestamp) + " [ms] "
        )

        assertEquals(SystemFileSystem.metadataOrNull(out)?.size, fid.size())
        SystemFileSystem.delete(out)

        client.shutdown()
        server.shutdown()
        halo.delete()
    }
}
