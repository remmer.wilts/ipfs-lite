package tech.lp2p.dark

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.newHalo
import kotlin.test.Test
import kotlin.test.assertTrue

class FetchStressDataTest {
    @Test
    fun stressFetchData(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val iterations = 100
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val client = newDark()
        repeat(iterations) {
            val data = TestEnv.getRandomBytes(SPLITTER_SIZE.toInt())
            val raw = halo.storeData(data)

            val request =
                createRequest(
                    TestEnv.loopbackPeeraddr(server.peerId(), serverPort),
                    raw.cid()
                )

            val cmp = client.fetchData(request)
            assertTrue(data.contentEquals(cmp))
        }
        client.shutdown()
        server.shutdown()
        halo.delete()
    }
}
