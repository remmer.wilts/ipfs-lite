package tech.lp2p.halo

import junit.framework.TestCase
import junit.framework.TestCase.assertFalse
import kotlinx.coroutines.runBlocking
import kotlinx.io.readByteArray
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class AddTest {
    @Test(expected = Exception::class)
    fun add_and_remove(): Unit = runBlocking {
        val halo = newHalo()
        val content = "Hello cat"
        val node = halo.storeText(content)
        assertEquals(content.toByteArray().size, node.size().toInt())


        assertTrue(halo.hasBlock(node.cid()))

        val text = node.cid()

        assertTrue(halo.hasBlock(text))
        halo.delete(node)
        assertFalse(halo.hasBlock(text))

        try {
            halo.fetchBlock(node.cid()) // closed exception expected
        } finally {
            halo.delete()
        }
    }


    @Test
    fun create_text() {
        val halo = newHalo()
        val content1 = "Pa gen fichye ki make pou efase"
        val text1 = TestEnv.createContent(
            halo, "Pa gen fichye ki make pou efase.txt",
            "text/plain", content1.toByteArray()
        )
        assertNotNull(text1)

        val content2 = "Non pa valab"
        val text2 = TestEnv.createContent(
            halo, "Non pa valab.txt",
            "text/plain",
            content2.toByteArray()
        )
        assertNotNull(text2)
        halo.delete()
    }

    @Test
    fun add_wrap(): Unit = runBlocking {
        val halo = newHalo()
        val packetSize = 1000
        val maxData = 1000
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            TestEnv.getRandomBytes(maxData * packetSize)
        )

        assertNotNull(fid)
        assertTrue(fid.name().isNotEmpty())

        val channel = halo.channel(fid)
        val bytes = channel.readAllBytes()
        assertEquals(bytes.size.toLong(), fid.size())
        halo.delete()
    }


    @Test
    fun add(): Unit = runBlocking {
        val packetSize = 1000
        val maxData = 1000

        val halo = newHalo()
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            TestEnv.getRandomBytes(maxData * packetSize)
        )


        assertNotNull(fid)
        assertEquals(fid.mimeType(), OCTET_MIME_TYPE)

        val channel = halo.channel(fid)
        assertEquals(fid.size(), channel.readAllBytes().size.toLong())

        halo.delete()
    }


    @Test
    fun add_wrap_small(): Unit = runBlocking {
        val packetSize = 200
        val maxData = 1000

        val halo = newHalo()
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            TestEnv.getRandomBytes(maxData * packetSize)
        )


        val channel = halo.channel(fid)
        assertEquals(fid.size(), channel.readAllBytes().size.toLong())

        halo.delete()
    }

    @Test
    fun createFile(): Unit = runBlocking {
        val packetSize = 200
        val maxData = 1000

        val halo = newHalo()
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            TestEnv.getRandomBytes(maxData * packetSize)
        )


        val channel = halo.channel(fid)
        assertEquals(channel.readAllBytes().size.toLong(), fid.size())
        halo.delete()
    }


    @Test
    fun test_inputStream(): Unit = runBlocking {
        val halo = newHalo()
        val text = "moin zehn"
        val node = halo.storeText(text)
        assertTrue(halo.hasBlock(node.cid()))

        val bytes = halo.fetchData(node)
        assertNotNull(bytes)
        assertEquals(bytes.size, text.length)
        assertEquals(text, bytes.toString(Charsets.UTF_8))
        halo.delete()
    }


    @Test
    fun test_inputStreamBig(): Unit = runBlocking {
        val halo = newHalo()
        val text = TestEnv.getRandomBytes((SPLITTER_SIZE.toInt() * 2) - 50)
        val fid = TestEnv.createContent(halo, "random.bin", OCTET_MIME_TYPE, text)
        val channel = halo.channel(fid)
        assertTrue(text.contentEquals(channel.readAllBytes()))
        halo.delete()
    }

    @Test
    fun test_reader(): Unit = runBlocking {
        val halo = newHalo()
        val text = "0123456789 jjjjjjjj"

        val fid = TestEnv.createContent(halo, "text.txt", OCTET_MIME_TYPE, text.toByteArray())
        assertTrue(halo.hasBlock(fid.cid()))

        val channel = createChannel(fid, halo)

        channel.seek(0)
        var buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(text, buffer.readByteArray().toString(Charsets.UTF_8))

        var pos = 11
        channel.seek(pos.toLong())
        buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(text.substring(pos), buffer.readByteArray().toString(Charsets.UTF_8))

        pos = 5
        channel.seek(pos.toLong())
        buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(text.substring(pos), buffer.readByteArray().toString(Charsets.UTF_8))
        halo.delete()
    }

    @Test
    fun test_readerBig(): Unit = runBlocking {
        val chunkData = SPLITTER_SIZE.toInt()
        val halo = newHalo()
        val text = TestEnv.getRandomBytes((chunkData * 2) - 50)
        val fid = TestEnv.createContent(halo, "random.bin", OCTET_MIME_TYPE, text)

        assertTrue(halo.hasBlock(fid.cid()))

        val channel = halo.channel(fid)


        channel.seek(0)
        var buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(chunkData, buffer.size.toInt())
        buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(chunkData - 50, buffer.size.toInt())

        var pos = chunkData + 50
        channel.seek(pos.toLong())
        buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(chunkData - 100, buffer.size.toInt())

        var data = buffer.readByteArray()
        assertEquals(chunkData - 100, data.size)
        assertEquals(0, buffer.size.toInt())

        pos = chunkData - 50
        channel.seek(pos.toLong())
        buffer = channel.next()
        assertNotNull(buffer)
        assertEquals(50, buffer.size.toInt())
        data = buffer.readByteArray()
        assertEquals(50, data.size)

        halo.delete()
    }


    @Test
    fun createRawText(): Unit = runBlocking {
        val halo = newHalo()

        val name = "Домашняя страница"
        val node = halo.storeText(name)
        TestCase.assertNotNull(node)

        TestCase.assertEquals(halo.fetchText(node), name)
        halo.delete()
    }


}