package tech.lp2p.halo

import kotlinx.coroutines.runBlocking
import tech.lp2p.halo.core.encodeRaw
import kotlin.math.abs
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.fail

class CatTest {
    @Test
    fun catNotExist(): Unit = runBlocking {
        val cid = abs(Random.nextLong())
        val halo = newHalo()
        try {

            halo.fetchBlock(cid)
            fail()

        } catch (_: Exception) {
            // ignore
        } finally {
            halo.delete()
        }

    }


    @Test
    fun catLocalTest(): Unit = runBlocking {
        val halo = newHalo()
        val local = halo.storeText("Moin Moin Moin")
        assertNotNull(local)
        val content = halo.fetchData(local)
        assertNotNull(content)

        val buffer = encodeRaw(content)

        val node = decodeNode(local.cid(), buffer)
        assertNotNull(node)
        assertEquals(node.cid(), local.cid())
        halo.delete()
    }
}