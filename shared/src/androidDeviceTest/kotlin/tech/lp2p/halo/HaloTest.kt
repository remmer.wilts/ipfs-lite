package tech.lp2p.halo

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.io.buffered
import kotlinx.io.files.SystemFileSystem
import tech.lp2p.halo.TestEnv.getRandomBytes
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import tech.lp2p.halo.core.Raw
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue
import kotlin.test.fail


class HaloTest {


    @Test
    fun resetTest() {

        val halo = newHalo()
        assertNotNull(halo.root())
        halo.reset()
        assertNotNull(halo.root())
        val root = halo.root() as Raw
        assertEquals(root.data().size, 0)
        assertNotNull(root.cid())
        halo.delete()

    }



    @Test
    fun fetchDataTest(): Unit = runBlocking {
        val halo = newHalo()
        val test = "Moin"
        val cid = halo.storeText(test)
        assertNotNull(cid)
        val bytes = halo.fetchData(cid)
        assertNotNull(bytes)
        assertEquals(test, String(bytes))
        try {
            val fault = TestEnv.randomLong()
            halo.fetchBlock(fault)
            fail()
        } catch (_: Exception) {
            // ok
        }
        halo.delete()
    }

    @Test
    fun fetchInfoTest(): Unit = runBlocking {
        val halo = newHalo()
        val test = "Moin"
        val cid = halo.storeText(test)
        assertNotNull(cid)

        var node = halo.info(cid.cid())
        assertEquals(node.cid(), cid.cid())


        val temp = halo.tempFile()
        val iteration = 10
        SystemFileSystem.sink(temp).buffered().use { source ->
            repeat(iteration) {
                source.write(getRandomBytes(SPLITTER_SIZE.toInt()))
            }
        }
        // (1) store the file
        val fid = halo.storeFile(temp, OCTET_MIME_TYPE)

        node = halo.info(fid.cid())
        assertEquals(node.cid(), fid.cid())
        halo.delete()
    }

    @Test
    fun rawCid() {
        val halo = newHalo()
        val cid = halo.storeText("hallo")
        assertNotNull(cid)
        halo.delete()
    }

    @Test
    fun textProgressTest(): Unit = runBlocking {
        val halo = newHalo()
        val test = "Moin bla bla"
        val cid = halo.storeText(test)
        assertNotNull(cid)

        val bytes = halo.fetchData(cid)
        assertNotNull(bytes)
        assertEquals(test, String(bytes))

        val text = halo.fetchText(cid)
        assertNotNull(text)
        assertEquals(test, text)
        halo.delete()
    }


    @Test
    fun compareFileSizes(): Unit = runBlocking {
        val halo = newHalo()
        val fid = TestEnv.createContent(halo, 100)
        assertNotNull(fid)
        val temp = halo.tempFile()
        halo.transferTo(fid, temp)

        assertEquals(SystemFileSystem.metadataOrNull(temp)?.size, fid.size())

        SystemFileSystem.delete(temp)
        halo.delete(fid)
        halo.delete()
    }

    @Test
    fun response(): Unit = runBlocking {
        val halo = newHalo()
        val fid = TestEnv.createContent(halo, 100)
        assertNotNull(fid)
        val temp = halo.tempFile()
        halo.transferTo(fid, temp)

        assertEquals(SystemFileSystem.metadataOrNull(temp)?.size, fid.size())


        val response = halo.response(fid.cid())
        assertNotNull(response)
        assertEquals(response.mimeType, fid.mimeType())
        assertEquals(response.reason, "OK")
        assertEquals(response.status, 200)
        assertEquals(response.headers.size, 3)
        assertEquals(response.encoding, Charsets.UTF_8.name())
        assertNotNull(response.channel)


        SystemFileSystem.delete(temp)
        halo.delete(fid)
        halo.delete()
    }

    @Test
    fun performanceContentReadWrite(): Unit = runBlocking {
        val halo = newHalo()
        val packetSize = 10000
        val maxData = 25000


        val inputFile = halo.tempFile()
        SystemFileSystem.sink(inputFile).buffered().use { source ->
            repeat(maxData) {
                source.write(getRandomBytes(packetSize))
            }
        }

        val size = SystemFileSystem.metadataOrNull(inputFile)?.size
        val expected = packetSize * maxData.toLong()
        assertEquals(expected, size)

        val fid = halo.storeFile(inputFile, OCTET_MIME_TYPE)
        assertNotNull(fid)

        val temp = halo.tempFile()

        halo.transferTo(fid, temp)
        assertEquals(SystemFileSystem.metadataOrNull(temp)?.size, size)


        val outputFile1 = halo.tempFile()
        halo.transferTo(fid, outputFile1)


        val outputFile2 = halo.tempFile()
        halo.transferTo(fid, outputFile2)


        assertEquals(SystemFileSystem.metadataOrNull(outputFile2)?.size, size)
        assertEquals(SystemFileSystem.metadataOrNull(outputFile1)?.size, size)

        // cleanup
        SystemFileSystem.delete(temp)
        SystemFileSystem.delete(outputFile2)
        SystemFileSystem.delete(outputFile1)
        SystemFileSystem.delete(inputFile)

        halo.delete(fid)
        halo.delete()
    }

    @Test
    fun contentWrite() {
        val halo = newHalo()
        val data = 1000000
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            getRandomBytes(data)
        )

        val size = fid.size()

        assertEquals(data, size.toInt())
        halo.delete()
    }


    @Test
    fun storeInputStream(): Unit = runBlocking {


        val halo = newHalo()

        val data = 100000
        val fid = TestEnv.createContent(
            halo, "test.bin", OCTET_MIME_TYPE,
            getRandomBytes(data)
        )

        val size = fid.size()


        assertEquals(fid.size(), size)

        val temp = halo.tempFile()
        halo.transferTo(fid, temp)

        assertEquals(SystemFileSystem.metadataOrNull(temp)?.size, size)

        // cleanup
        SystemFileSystem.delete(temp)
        halo.delete()
    }

    @Test
    fun testString(): Unit = runBlocking {
        val halo = newHalo()
        val text = "Hello Moin und Zehn Elf"
        val raw = halo.storeText(text)
        assertNotNull(raw)

        val result = halo.fetchData(raw)
        assertNotNull(result)
        assertEquals(text, String(result))
        halo.delete()
    }

    @Test
    fun reopenFileStore(): Unit = runBlocking {

        val halo = newHalo()
        assertNotNull(halo)

        val textTest = "Hello World now"
        val raw = halo.storeText(textTest)
        assertNotNull(raw)

        val dataTest = getRandomBytes(1000)

        val data = halo.storeData(dataTest)
        assertNotNull(data)

        val haloCmp = newHalo(halo.directory())
        assertNotNull(haloCmp)

        val testCheck = haloCmp.fetchText(raw)
        assertEquals(testCheck, textTest)

        val dataCheck = haloCmp.fetchData(data)
        assertTrue(dataCheck.contentEquals(dataTest))

        halo.delete()
        haloCmp.delete()
    }


    @Test
    fun lotsOfData(): Unit = runBlocking {


        val halo = newHalo()
        assertNotNull(halo)

        val instances = 10000
        val nodes = mutableListOf<Node>()
        var timestamp = System.currentTimeMillis()
        for (i in 0 until instances) {
            val data = halo.storeText("test$i")
            assertNotNull(data)
            nodes.add(data)
        }
        println(
            "Time Writing lotsOfData " +
                    (System.currentTimeMillis() - timestamp) + " [ms]"
        )


        timestamp = System.currentTimeMillis()
        for (i in 0 until instances) {
            val text = halo.fetchText(nodes[i])
            assertEquals(text, "test$i")
        }
        println(
            "Time Reading lotsOfData " +
                    (System.currentTimeMillis() - timestamp) + " [ms]"
        )

        halo.delete()
    }

    @Test
    fun testParallel(): Unit = runBlocking {
        val halo = newHalo()
        val data = getRandomBytes(SPLITTER_SIZE.toInt())
        val node = halo.storeData(data)
        assertNotNull(node)


        val finished = atomic(0)
        val instances = 100

        runBlocking(Dispatchers.IO) {
            repeat(instances) {
                launch {
                    try {
                        val output = halo.fetchData(node)
                        assertTrue(data.contentEquals(output))
                        finished.incrementAndGet()
                    } catch (throwable: Throwable) {
                        TestEnv.error(throwable)
                        fail()
                    }
                }
            }
        }


        assertEquals(finished.value.toLong(), instances.toLong())
        halo.delete()
    }
}