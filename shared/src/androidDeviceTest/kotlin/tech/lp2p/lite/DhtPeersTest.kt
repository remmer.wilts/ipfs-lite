package tech.lp2p.lite

import kotlinx.coroutines.runBlocking
import tech.lp2p.asen.Key
import tech.lp2p.asen.PeerId
import tech.lp2p.lite.protos.DhtPeer
import tech.lp2p.lite.protos.DhtPeers
import tech.lp2p.lite.protos.createDhtPeer
import tech.lp2p.lite.protos.createKey
import java.net.InetAddress
import java.net.UnknownHostException
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class DhtPeersTest {
    @Test
    fun saturationModeNotReplaced(): Unit = runBlocking {
        val dhtPeers = DhtPeers(2)
        assertNotNull(dhtPeers)

        val key = createKey("Moin".toByteArray())
        dhtPeers.enhance(random(key))
        dhtPeers.enhance(random(key))
        assertEquals(dhtPeers.size().toLong(), 2)

        dhtPeers.enhance(perfect(key))
        assertTrue(dhtPeers.size() >= 2) // can be three
    }

    @Test
    fun saturationModeReplaced(): Unit = runBlocking {
        val dhtPeers = DhtPeers(2)
        assertNotNull(dhtPeers)

        val key = createKey("Moin".toByteArray())
        val a = random(key)
        dhtPeers.enhance(a)
        val b = random(key)
        dhtPeers.enhance(b)
        assertEquals(dhtPeers.size().toLong(), 2)

        a.done()
        b.done()

        dhtPeers.enhance(perfect(key))
        assertEquals(dhtPeers.size().toLong(), 2) // can be three
    }

    @Test
    fun random(): Unit = runBlocking {
        val dhtPeers = DhtPeers(2)
        assertNotNull(dhtPeers)

        val key = createKey("Moin".toByteArray())
        repeat(20) {
            val a = random(key)
            dhtPeers.enhance(a)
            a.done()
        }

        dhtPeers.enhance(perfect(key))
        assertEquals(dhtPeers.size().toLong(), 2)
    }


    @Test
    fun fill(): Unit = runBlocking {
        val dhtPeers = DhtPeers(2)
        assertNotNull(dhtPeers)

        val key = createKey("Moin".toByteArray())
        repeat(20) {
            val a = random(key)
            dhtPeers.fill(a)
            a.done()
        }

        assertEquals(dhtPeers.size().toLong(), 2)


        // check order
        val first = dhtPeers.first()
        assertNotNull(first)
        val last = dhtPeers.last()
        assertNotNull(last)

        val res = first.compareTo(last)
        // if value is negative first is closer to key (which is the way)
        assertTrue(res < 0)
    }


    @Throws(UnknownHostException::class)
    private fun random(key: Key): DhtPeer = runBlocking {
        val random = TestEnv.randomPeerId()
        val peeraddr = createPeeraddr(
            random,
            InetAddress.getByName("139.178.68.146").address, 4001.toUShort()
        )

        return@runBlocking createDhtPeer(peeraddr, false, key)
    }

    private fun perfect(key: Key): DhtPeer = runBlocking {
        val random = PeerId(key.hash)
        val peeraddr = createPeeraddr(
            random,
            InetAddress.getByName("139.178.68.146").address, 4001.toUShort()
        )
        return@runBlocking createDhtPeer(peeraddr, false, key)
    }
}
