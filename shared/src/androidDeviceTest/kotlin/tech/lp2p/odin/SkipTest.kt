package tech.lp2p.odin

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.io.Buffer
import tech.lp2p.dark.core.EOF
import tech.lp2p.dark.createRequest
import tech.lp2p.dark.newDark
import tech.lp2p.halo.SPLITTER_SIZE
import tech.lp2p.halo.core.OCTET_MIME_TYPE
import tech.lp2p.halo.newHalo
import java.io.IOException
import kotlin.random.Random
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.fail

class SkipTest {
    @Test
    fun skip() {
        val halo = newHalo()
        val data = Random.nextBytes(10 * SPLITTER_SIZE.toInt())
        val skip = (data.size * 0.9f).toLong()
        val source = Buffer()
        source.write(data)

        val fid = halo.storeSource(source, "test.bin", OCTET_MIME_TYPE)

        val channel = halo.channel(fid)

        Stream(channel).use { stream ->
            val available = stream.available().toLong()
            assertEquals(available, data.size.toLong())
            val skipped = stream.skip(skip)
            assertEquals(skipped, skip)
            val rest = ByteArray((data.size * 0.1).toInt())
            var read = 0
            do {
                read += stream.read(rest, read, rest.size)
            } while (read < rest.size)
            assertEquals(read.toLong(), rest.size.toLong())
            val eof = stream.read()
            assertEquals(eof.toLong(), -1) // end of file
        }

        halo.delete()
    }


    @Test
    fun closeClient(): Unit = runBlocking {
        val serverPort = TestEnv.nextFreePort()
        val halo = newHalo()
        val server = newDark()
        server.runService(halo, serverPort)
        val fid = TestEnv.createContent(halo, 10000)
        assertNotNull(fid)

        val loopback = TestEnv.loopbackPeeraddr(server.peerId(), serverPort)
        val uri = createRequest(loopback, fid)


        val client = newDark()

        launch {
            val response = client.response(uri)
            assertNotNull(response)
            val stream = Stream(response.channel)
            try {
                var i = 0
                while (stream.read() != EOF) {
                    i++
                }
            } catch (_: IOException) {
                // this is expected
            } catch (throwable: Throwable) {
                fail(throwable.message) // other exceptions not expected
            }
        }

        val wait = atomic(false)
        launch {
            delay(500)
            client.shutdown()
            wait.value = true
        }

        while (wait.value == false) {
            delay(50)
        }

        server.shutdown()
        halo.delete()
    }
}