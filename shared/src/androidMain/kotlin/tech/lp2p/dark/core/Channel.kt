package tech.lp2p.dark.core

import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.io.Buffer
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.debug
import tech.lp2p.halo.HALO_ROOT
import tech.lp2p.lite.verify
import java.net.Socket

class Channel(
    val remotePeeraddr: Peeraddr,
    private val connector: Connector,
    private val socket: Socket
) {
    val mutex = Mutex()

    suspend fun request(cid: Long): Buffer {
        val cidRequest = (cid == HALO_ROOT)

        mutex.withLock {
            try {
                val outputStream = socket.getOutputStream()
                outputStream.write(longToBytes(cid))
                outputStream.flush()


                val inputStream = socket.getInputStream()

                val data = ByteArray(Int.SIZE_BYTES)

                var read = inputStream.read(data)
                check(read == Int.SIZE_BYTES)
                val length = bytesToInt(data)
                check(length.toInt() != EOF) { "EOF" }

                if (cidRequest) {
                    val root = ByteArray(Long.SIZE_BYTES)
                    read = inputStream.read(root)
                    check(read != EOF) { "EOF" }
                    require(read == Long.SIZE_BYTES) { "Not all root data read" }

                    // read an verify signature
                    val lengthSignature = inputStream.read()
                    check(lengthSignature != EOF) { "EOF" }

                    val signature = ByteArray(lengthSignature.toInt())
                    read = inputStream.read(signature)
                    require(read == lengthSignature.toInt()) { "Not all signature data read" }
                    verify(remotePeeraddr.peerId, root, signature)

                    val payload = Buffer()
                    payload.writeLong(bytesToLong(root))
                    return payload
                } else {
                    read = 0
                    val content = ByteArray(length.toInt()) // TODO VERY SLOW !!!
                    do {
                        val iterRead = inputStream.read(content, read, (length.toInt() - read))
                        if (iterRead <= EOF) {
                            break
                        }
                        read += iterRead
                    } while (read < length.toInt())

                    check(read == length.toInt()) { "Service data unavailable" }
                    val payload = Buffer()
                    payload.write(content)
                    check(payload.size.toInt() == length.toInt()) { "Service data unavailable" }

                    return payload
                }
            } catch (throwable: Throwable) {
                close()
                throw throwable
            }
        }
    }

    fun close() {
        try {
            socket.close()
        } catch (throwable: Throwable) {
            debug(throwable)
        } finally {
            connector.removeChannel(this)
        }
    }


    val isConnected: Boolean
        get() = !socket.isClosed

    fun remotePeerId(): PeerId {
        return remotePeeraddr.peerId
    }
}
