package tech.lp2p.lite.protos

import tech.lp2p.lite.quic.Handler
import tech.lp2p.lite.quic.Stream

class StreamHandler : Handler {
    override suspend fun protocol(stream: Stream) {
        // nothing to do here
    }


    override suspend fun data(stream: Stream, data: ByteArray) {
        if (data.isNotEmpty()) {
            throw Exception("not expected data received for multistream")
        }
    }
}