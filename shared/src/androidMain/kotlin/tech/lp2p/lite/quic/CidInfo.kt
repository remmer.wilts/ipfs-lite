package tech.lp2p.lite.quic


internal class CidInfo(
    private val sequenceNumber: Int,
    @field:Volatile private var cid: Number,
    @field:Volatile private var cidStatus: CidStatus
) :
    Comparable<CidInfo> {
    fun sequenceNumber(): Int {
        return sequenceNumber
    }

    fun cid(): Number {
        return cid
    }

    fun cidStatus(): CidStatus {
        return cidStatus
    }

    fun cidStatus(newStatus: CidStatus) {
        cidStatus = newStatus
    }

    override fun compareTo(other: CidInfo): Int {
        return sequenceNumber.compareTo(other.sequenceNumber)
    }

    fun cid(cid: Number) {
        this.cid = cid
    }
}

