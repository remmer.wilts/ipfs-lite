package tech.lp2p.lite.quic

enum class HandshakeState {
    Initial,
    HasHandshakeKeys,
    HasAppKeys,
    Confirmed;

    fun transitionAllowed(proposedState: HandshakeState): Boolean {
        return this.ordinal < proposedState.ordinal
    }
}

