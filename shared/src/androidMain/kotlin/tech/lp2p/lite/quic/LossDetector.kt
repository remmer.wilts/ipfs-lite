package tech.lp2p.lite.quic

import kotlinx.atomicfu.atomic
import kotlinx.atomicfu.updateAndGet
import tech.lp2p.lite.quic.FrameReceived.AckFrame
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.max

internal class LossDetector(private val connectionFlow: ConnectionFlow) {
    private val packetSentLog: MutableMap<Long, PacketStatus> = ConcurrentHashMap()
    private val largestAcked = atomic(-1L)

    @Volatile
    private var isStopped = false

    fun packetSent(packetStatus: PacketStatus) {
        if (isStopped) {
            return
        }

        // During a reset operation, no new packets must be logged as sent.
        packetSentLog[packetStatus.packet.packetNumber()] = packetStatus
    }

    fun processAckFrameReceived(ackFrame: AckFrame, timeReceived: Long) {
        if (isStopped) {
            return
        }

        largestAcked.updateAndGet { operand ->
            max(operand, ackFrame.largestAcknowledged)
        }


        var newlyAcked: PacketStatus? = null

        // it is ordered, packet status with highest packet number is at the top
        val acknowledgedRanges = ackFrame.acknowledgedRanges
        var i = 0
        while (i < acknowledgedRanges.size) {
            val to = acknowledgedRanges[i]
            i++
            val from = acknowledgedRanges[i]
            for (pn in to downTo from) {
                val packetStatus = packetSentLog[pn]
                if (packetStatus != null) {
                    packetSentLog.remove(pn) // remove entry
                    if (isAckEliciting(packetStatus.packet)) {
                        connectionFlow.processAckedPacket(packetStatus)
                    }
                    if (newlyAcked == null) {
                        newlyAcked = packetStatus
                    }
                }
            }
            i++
        }

        // https://tools.ietf.org/html/draft-ietf-quic-recovery-33#section-5.1
        // "An endpoint generates an RTT sample on receiving an ACK frame that meets the following
        // two conditions:
        // the largest acknowledged packet number is newly acknowledged, and
        // at least one of the newly acknowledged packets was ack-eliciting."
        if (newlyAcked != null) {
            if (newlyAcked.packet.packetNumber() == ackFrame.largestAcknowledged) {
                if (isAckEliciting(newlyAcked.packet)) { // at least one of the newly acknowledged packets was ack-eliciting."
                    connectionFlow.addSample(timeReceived, newlyAcked.timeSent, ackFrame.ackDelay)
                }
            }
        }
    }

    fun stop() {
        isStopped = true

        for (packetStatus in packetSentLog.values) {
            connectionFlow.discardBytesInFlight(packetStatus)
        }

        packetSentLog.clear()
    }

    suspend fun detectLostPackets() {
        if (isStopped) {
            return
        }

        val lossDelay = (Settings.TIME_THRESHOLD * max(
            connectionFlow.getSmoothedRtt(), connectionFlow.getLatestRtt()
        )).toInt()

        val lostSendTime = System.currentTimeMillis() - lossDelay


        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-6.1
        // "A packet is declared lost if it meets all the following conditions:
        //   o  The packet is unacknowledged, in-flight, and was sent prior to an
        //      acknowledged packet.
        //   o  Either its packet number is kPacketThreshold smaller than an
        //      acknowledged packet (Section 6.1.1), or it was sent long enough in
        //      the past (Section 6.1.2)."
        // https://tools.ietf.org/html/draft-ietf-quic-recovery-20#section-2
        // "In-flight:  Packets are considered in-flight when they have been sent
        //      and neither acknowledged nor declared lost, and they are not ACK-
        //      only."
        for (packetStatus in packetSentLog.values) {
            if (pnTooOld(packetStatus) || sentTimeTooLongAgo(packetStatus, lostSendTime)) {
                if (!packetStatus.packet.isAckOnly) {
                    declareLost(packetStatus)
                }
            }
        }
    }

    private fun pnTooOld(p: PacketStatus): Boolean {
        val kPacketThreshold = 3
        return p.packet.packetNumber() <= largestAcked.value - kPacketThreshold
    }

    private fun sentTimeTooLongAgo(p: PacketStatus, lostSendTime: Long): Boolean {
        return p.packet.packetNumber() <= largestAcked.value && p.timeSent < lostSendTime
    }

    private suspend fun declareLost(packetStatus: PacketStatus) {
        if (isAckEliciting(packetStatus.packet)) {
            connectionFlow.registerLost(packetStatus)
        }

        // Retransmitting the frames in the lost packet is delegated to the lost frame callback,
        // because whether retransmitting the frame is necessary (and in which manner) depends
        // on frame payloadType,
        // see https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-13.3
        val frames = packetStatus.packet.frames()
        for (frame in frames) {
            when (frame.frameType) {
                FrameType.MaxStreamsFrame, FrameType.MaxStreamDataFrame, FrameType.StopSendingFrame, FrameType.ResetStreamFrame, FrameType.RetireConnectionIdFrame, FrameType.StreamFrame, FrameType.MaxDataFrame, FrameType.HandshakeDoneFrame -> connectionFlow.insertRequest(
                    packetStatus.packet.level(),
                    frame
                )

                FrameType.CryptoFrame,
                FrameType.NewConnectionIdFrame,
                FrameType.DataBlockedFrame,
                FrameType.StreamDataBlockedFrame -> connectionFlow.addRequest(
                    packetStatus.packet.level(),
                    frame
                )

                else -> {}
            }
        }

        packetSentLog.remove(packetStatus.packet.packetNumber())
    }


}
