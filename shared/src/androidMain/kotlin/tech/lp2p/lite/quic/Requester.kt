package tech.lp2p.lite.quic

import kotlinx.io.Buffer
import tech.lp2p.lite.quic.AlpnRequester.AlpnState


interface Requester {

    suspend fun data(stream: Stream, data: ByteArray)

    fun done()


    data class RequestResponse(val stream: Stream) : StreamHandler {
        override fun terminated() {
        }

        override fun fin() {
        }

        override fun readFully(): Boolean {
            return true
        }

        override suspend fun data(data: Buffer) {
            throw IllegalStateException("should never be invoked")
        }
    }

    companion object {

        suspend fun createStream(connection: Connection, requester: Requester): Stream {
            return connection.createStream({ stream: Stream ->
                AlpnRequester(stream, requester, AlpnState(requester))
            }, true)
        }


        suspend fun createStream(connection: Connection): Stream {
            return connection.createStream({ stream: Stream -> RequestResponse(stream) }, true)
        }
    }
}
