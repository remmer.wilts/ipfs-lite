package tech.lp2p.lite.quic

import tech.lp2p.lite.quic.AlpnResponder.Libp2pState

data class Responder(val protocols: Protocols) {
    suspend fun protocol(stream: Stream, protocol: String) {
        val handler = protocols[protocol]
        if (handler != null) {
            handler.protocol(stream)
        } else {
            stream.resetStream(Settings.PROTOCOL_NEGOTIATION_FAILED.toLong())
        }
    }


    suspend fun data(stream: Stream, protocol: String, data: ByteArray) {
        val handler = protocols[protocol]
        if (handler != null) {
            handler.data(stream, data)
        } else {
            stream.resetStream(Settings.PROTOCOL_NEGOTIATION_FAILED.toLong())
        }
    }


    fun createResponder(stream: Stream): AlpnResponder {
        return AlpnResponder(stream, this, Libp2pState(this))
    }
}