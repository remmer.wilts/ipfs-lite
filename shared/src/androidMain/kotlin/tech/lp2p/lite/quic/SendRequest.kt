package tech.lp2p.lite.quic


interface FrameSupplier {
    suspend fun nextFrame(maxSize: Int): Frame?
}

data class SendRequest(val estimatedSize: Int, val frameSupplier: FrameSupplier)

