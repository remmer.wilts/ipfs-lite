package tech.lp2p.lite.quic

import kotlinx.io.Buffer

interface StreamHandler {
    fun terminated()

    fun fin()

    fun readFully(): Boolean

    suspend fun data(data: Buffer)
}
