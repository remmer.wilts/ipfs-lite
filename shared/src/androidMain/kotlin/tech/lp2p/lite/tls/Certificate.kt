package tech.lp2p.lite.tls

import java.security.PrivateKey

interface Certificate {
    fun encoded(): ByteArray
    fun sigAlgName(): String
    fun certificateKey(): PrivateKey

}