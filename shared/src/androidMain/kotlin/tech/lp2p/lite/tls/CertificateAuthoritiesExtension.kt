/*
 * Copyright © 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import javax.security.auth.x500.X500Principal

// https://tools.ietf.org/html/rfc8446#section-4.2.4
data class CertificateAuthoritiesExtension(val authorities: Array<X500Principal>) : Extension {
    override fun getBytes(): ByteArray {
        val authoritiesLength =
            authorities.sumOf { x500principal: X500Principal -> x500principal.encoded.size }
        val extensionLength = authoritiesLength + authorities.size * 2 + 2 + 4
        val buffer = Buffer()

        buffer.writeShort(ExtensionType.CERTIFICATE_AUTHORITIES.value)
        buffer.writeShort((extensionLength - 4).toShort())
        buffer.writeShort((extensionLength - 6).toShort())
        for (authority in authorities) {
            buffer.writeShort(authority.encoded.size.toShort())
            buffer.write(authority.encoded)
        }
        require(buffer.size == extensionLength.toLong())
        return buffer.readByteArray()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CertificateAuthoritiesExtension

        return authorities.contentEquals(other.authorities)
    }

    override fun hashCode(): Int {
        return authorities.contentHashCode()
    }

    companion object {

        fun parse(buffer: Buffer, extensionLength: Int): CertificateAuthoritiesExtension {
            val authorities: MutableList<X500Principal> = ArrayList()
            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, 2
            )

            val authoritiesLength = buffer.readShort().toInt()
            if (extensionDataLength != authoritiesLength + 2) {
                throw DecodeErrorException("inconsistent length fields")
            }

            var remaining = authoritiesLength
            while (remaining > 0) {
                if (remaining < 2) {
                    throw DecodeErrorException("inconsistent length fields")
                }
                remaining -= 2
                val dnLength = buffer.readShort().toInt() and 0xffff
                if (dnLength > remaining) {
                    throw DecodeErrorException("inconsistent length fields")
                }
                if (dnLength <= buffer.size) {
                    val dn = buffer.readByteArray(dnLength)

                    remaining -= dnLength
                    try {
                        authorities.add(X500Principal(dn))
                    } catch (_: IllegalArgumentException) {
                        throw DecodeErrorException("authority not in DER format")
                    }
                } else {
                    throw DecodeErrorException("inconsistent length fields")
                }
            }

            return CertificateAuthoritiesExtension(authorities.toTypedArray())
        }
    }
}
