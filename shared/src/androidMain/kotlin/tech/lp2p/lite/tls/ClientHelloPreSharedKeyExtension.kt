/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * TLS Pre-Shared Key Extension, ClientHello variant.
 * see [...](https://datatracker.ietf.org/doc/html/rfc8446#section-4.2.11)
 */
data class ClientHelloPreSharedKeyExtension(
    val identities: Array<PskIdentity>,
    val binders: Array<PskBinderEntry>,
    val binderPosition: Int
) : PreSharedKeyExtension {
    override fun getBytes(): ByteArray {
        throw IllegalArgumentException()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ClientHelloPreSharedKeyExtension

        if (!identities.contentEquals(other.identities)) return false
        if (!binders.contentEquals(other.binders)) return false
        if (binderPosition != other.binderPosition) return false

        return true
    }

    override fun hashCode(): Int {
        var result = identities.contentHashCode()
        result = 31 * result + binders.contentHashCode()
        result = 31 * result + binderPosition
        return result
    }


    data class PskIdentity(val identity: ByteArray, val obfuscatedTicketAge: Long) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as PskIdentity

            if (!identity.contentEquals(other.identity)) return false
            if (obfuscatedTicketAge != other.obfuscatedTicketAge) return false

            return true
        }

        override fun hashCode(): Int {
            var result = identity.contentHashCode()
            result = 31 * result + obfuscatedTicketAge.hashCode()
            return result
        }
    }


    data class PskBinderEntry(val hmac: ByteArray) {
        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as PskBinderEntry

            return hmac.contentEquals(other.hmac)
        }

        override fun hashCode(): Int {
            return hmac.contentHashCode()
        }
    }

    companion object {
        private const val MINIMUM_EXTENSION_DATA_SIZE = 2 + 2 + 1 + 4 + 2 + 1 + 32


        fun parse(buffer: Buffer, extensionLength: Int): ClientHelloPreSharedKeyExtension {

            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, MINIMUM_EXTENSION_DATA_SIZE
            )

            val identities: MutableList<PskIdentity> = ArrayList()
            var remainingIdentitiesLength = buffer.readShort().toInt() and 0xffff
            var remaining = extensionDataLength - 2
            while (remainingIdentitiesLength > 0) {
                if (remaining < 2) {
                    throw DecodeErrorException("Incomplete psk identity")
                }
                val identityLength = buffer.readShort().toInt() and 0xffff
                remaining -= 2
                if (identityLength > remaining) {
                    throw DecodeErrorException("Incorrect identity length value")
                }
                val identity = buffer.readByteArray(identityLength)

                remaining -= identityLength
                if (remaining < 4) {
                    throw DecodeErrorException("Incomplete psk identity")
                }
                val obfuscatedTicketAge = buffer.readInt()
                remaining -= 4
                identities.add(PskIdentity(identity, obfuscatedTicketAge.toLong()))
                remainingIdentitiesLength -= (2 + identityLength + 4)
            }
            if (remainingIdentitiesLength != 0) {
                throw DecodeErrorException("Incorrect identities length value")
            }

            val binderPosition = buffer.size
            val binders: MutableList<PskBinderEntry> = ArrayList()
            if (remaining < 2) {
                throw DecodeErrorException("Incomplete binders")
            }
            var bindersLength = buffer.readShort().toInt() and 0xffff
            remaining -= 2
            while (bindersLength > 0) {
                if (remaining < 1) {
                    throw DecodeErrorException("Incorrect binder value")
                }
                val binderLength = buffer.readByte().toInt() and 0xff
                remaining -= 1
                if (binderLength > remaining) {
                    throw DecodeErrorException("Incorrect binder length value")
                }
                if (binderLength < 32) {
                    throw DecodeErrorException("Invalid binder length")
                }
                val hmac = buffer.readByteArray(binderLength)

                remaining -= binderLength
                binders.add(PskBinderEntry(hmac))
                bindersLength -= (1 + binderLength)
            }
            if (bindersLength != 0) {
                throw DecodeErrorException("Incorrect binders length value")
            }
            if (remaining > 0) {
                throw DecodeErrorException("Incorrect extension data length value")
            }
            if (identities.size != binders.size) {
                throw DecodeErrorException("Inconsistent number of identities vs binders")
            }
            if (identities.isEmpty()) {
                throw DecodeErrorException("Empty OfferedPsks")
            }

            return ClientHelloPreSharedKeyExtension(
                identities.toTypedArray(),
                binders.toTypedArray(), binderPosition.toInt()
            )
        }
    }
}
