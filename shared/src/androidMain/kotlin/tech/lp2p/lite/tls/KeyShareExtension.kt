/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import java.math.BigInteger
import java.security.AlgorithmParameters
import java.security.KeyFactory
import java.security.PublicKey
import java.security.interfaces.ECPublicKey
import java.security.spec.ECGenParameterSpec
import java.security.spec.ECParameterSpec
import java.security.spec.ECPoint
import java.security.spec.ECPublicKeySpec

/**
 * The TLS "key_share" extension contains the endpoint's cryptographic parameters.
 * See [...](https://tools.ietf.org/html/rfc8446#section-4.2.8)
 */

data class KeyShareExtension(
    val handshakeType: HandshakeType,
    val keyShareEntries: Array<KeyShareEntry>
) : Extension {
    override fun getBytes(): ByteArray {
        val keyShareEntryLength =
            keyShareEntries
                .map { obj: KeyShareEntry -> obj.namedGroup() }
                .map { o: NamedGroup -> CURVE_KEY_LENGTHS[o]!! }.sumOf { s: Int -> 2 + 2 + s }
                .toShort()
        var extensionLength = keyShareEntryLength
        if (handshakeType == HandshakeType.CLIENT_HELLO) {
            extensionLength = (extensionLength + 2).toShort()
        }

        val buffer = Buffer()

        buffer.writeShort(ExtensionType.KEY_SHARE.value)
        buffer.writeShort(extensionLength) // Extension data length (in bytes)

        if (handshakeType == HandshakeType.CLIENT_HELLO) {
            buffer.writeShort(keyShareEntryLength)
        }

        for (keyShare in keyShareEntries) {
            buffer.writeShort(keyShare.namedGroup().value)
            buffer.writeShort(getCurveKeyLength(keyShare.namedGroup()))
            if (keyShare.namedGroup() == NamedGroup.SECP256r1) {
                // See https://tools.ietf.org/html/rfc8446#section-4.2.8.2, "For secp256r1, secp384r1, and secp521r1, ..."
                buffer.writeByte(4.toByte())
                val affineX = (keyShare.key() as ECPublicKey).w.affineX.toByteArray()
                writeAffine(buffer, affineX)
                val affineY = (keyShare.key() as ECPublicKey).w.affineY.toByteArray()
                writeAffine(buffer, affineY)
            } else {
                throw RuntimeException()
            }
        }
        require(buffer.size.toInt() == 4 + extensionLength)
        return buffer.readByteArray()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as KeyShareExtension

        if (handshakeType != other.handshakeType) return false
        if (!keyShareEntries.contentEquals(other.keyShareEntries)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = handshakeType.hashCode()
        result = 31 * result + keyShareEntries.contentHashCode()
        return result
    }

    interface KeyShareEntry {
        fun namedGroup(): NamedGroup

        fun key(): PublicKey
    }


    private data class BasicKeyShareEntry(
        val namedGroup: NamedGroup,
        val key: PublicKey
    ) : KeyShareEntry {
        override fun namedGroup(): NamedGroup {
            return namedGroup
        }

        override fun key(): PublicKey {
            return key
        }
    }


    private data class ECKeyShareEntry(
        val namedGroup: NamedGroup,
        val key: ECPublicKey
    ) : KeyShareEntry {
        override fun namedGroup(): NamedGroup {
            return namedGroup
        }

        override fun key(): PublicKey {
            return key
        }
    }

    companion object {
        private val supportedCurves: List<NamedGroup> = listOf(NamedGroup.SECP256r1)
        private val CURVE_KEY_LENGTHS: Map<NamedGroup, Int> = java.util.Map.of(
            NamedGroup.SECP256r1, 65
        )


        fun create(
            publicKey: PublicKey,
            ecCurve: NamedGroup,
            handshakeType: HandshakeType
        ): KeyShareExtension {
            if (!supportedCurves.contains(ecCurve)) {
                throw RuntimeException("Only curves supported: $supportedCurves")
            }
            val keyShareEntries = arrayOf<KeyShareEntry>(BasicKeyShareEntry(ecCurve, publicKey))
            return KeyShareExtension(handshakeType, keyShareEntries)
        }

        private fun createKeyShareExtension(
            buffer: Buffer, extensionLength: Int,
            handshakeType: HandshakeType
        ): KeyShareExtension {
            val keyShareEntries: MutableList<KeyShareEntry> = ArrayList()

            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, 1
            )
            if (extensionDataLength < 2) {
                throw DecodeErrorException("extension underflow")
            }

            if (handshakeType == HandshakeType.CLIENT_HELLO) {
                val keyShareEntriesSize = buffer.readShort().toInt()
                if (extensionDataLength != 2 + keyShareEntriesSize) {
                    throw DecodeErrorException("inconsistent length")
                }
                var remaining = keyShareEntriesSize
                while (remaining > 0) {
                    remaining -= parseKeyShareEntry(keyShareEntries, buffer)
                }
                if (remaining != 0) {
                    throw DecodeErrorException("inconsistent length")
                }
            } else if (handshakeType == HandshakeType.SERVER_HELLO) {
                var remaining = extensionDataLength
                remaining -= parseKeyShareEntry(keyShareEntries, buffer)
                if (remaining != 0) {
                    throw DecodeErrorException("inconsistent length")
                }
            } else {
                throw IllegalArgumentException()
            }

            return KeyShareExtension(
                handshakeType,
                keyShareEntries.toTypedArray()
            )
        }

        /**
         * Assuming KeyShareServerHello:
         * "In a ServerHello message, the "extension_data" field of this extension contains a KeyShareServerHello value..."
         */

        fun create(
            buffer: Buffer,
            extensionLength: Int,
            handshakeType: HandshakeType
        ): KeyShareExtension {
            return createKeyShareExtension(buffer, extensionLength, handshakeType)
        }

        private fun getCurveKeyLength(namedGroup: NamedGroup): Short {
            val length = CURVE_KEY_LENGTHS[namedGroup]
                ?: throw NullPointerException()
            return length.toShort()
        }

        private fun rawToEncodedECPublicKey(
            curveName: NamedGroup,
            rawBytes: ByteArray
        ): ECPublicKey {

            val kf = KeyFactory.getInstance("EC")
            val x = rawBytes.copyOfRange(0, rawBytes.size / 2)
            val y = rawBytes.copyOfRange(rawBytes.size / 2, rawBytes.size)
            val w = ECPoint(BigInteger(1, x), BigInteger(1, y))
            return kf.generatePublic(
                ECPublicKeySpec(
                    w,
                    ecParameterSpecForCurve(curveName)
                )
            ) as ECPublicKey

        }

        private fun ecParameterSpecForCurve(curveName: NamedGroup): ECParameterSpec {

            val params = AlgorithmParameters.getInstance("EC")
            params.init(ECGenParameterSpec(curveName.name.lowercase()))
            return params.getParameterSpec(ECParameterSpec::class.java)

        }


        private fun parseKeyShareEntry(
            keyShareEntries: MutableList<KeyShareEntry>,
            buffer: Buffer
        ): Int {
            var read = 0
            if (buffer.size < 4) {
                throw DecodeErrorException("extension underflow")
            }

            val namedGroupValue = buffer.readShort()
            read += 2
            val namedGroup: NamedGroup = NamedGroup.get(namedGroupValue)

            if (!supportedCurves.contains(namedGroup)) {
                throw RuntimeException("Curve '$namedGroup' not supported")
            }

            val keyLength = buffer.readShort().toInt()
            read += 2
            if (buffer.size < keyLength) {
                throw DecodeErrorException("extension underflow")
            }
            if (keyLength != getCurveKeyLength(namedGroup).toInt()) {
                throw DecodeErrorException("Invalid " + namedGroup.name + " key length: " + keyLength)
            }

            read += keyLength
            if (namedGroup == NamedGroup.SECP256r1) {
                val headerByte = buffer.readByte().toInt()
                if (headerByte == 4) {
                    val keyData = buffer.readByteArray(keyLength - 1)
                    val ecPublicKey = rawToEncodedECPublicKey(namedGroup, keyData)
                    keyShareEntries.add(ECKeyShareEntry(namedGroup, ecPublicKey))
                } else {
                    throw DecodeErrorException("EC keys must be in legacy form")
                }
            }
            return read
        }

        private fun writeAffine(buffer: Buffer, affine: ByteArray) {
            if (affine.size == 32) {
                buffer.write(affine)
            } else if (affine.size < 32) {
                val times = 32 - affine.size
                repeat(times) {
                    buffer.writeByte(0.toByte())
                }
                buffer.write(affine, 0, affine.size)
            } else {
                // affine.length > 32
                for (i in 0 until affine.size - 32) {
                    if (affine[i].toInt() != 0) {
                        throw RuntimeException("W Affine more then 32 bytes, leading bytes not 0 ")
                    }
                }
                buffer.write(affine, affine.size - 32, affine.size)
            }
        }
    }
}
