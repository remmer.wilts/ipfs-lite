/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

/**
 * The TLS supported groups extension.
 * See [...](https://tools.ietf.org/html/rfc8446#section-4.2.7)
 */
data class SupportedGroupsExtension(val namedGroups: Array<NamedGroup>) : Extension {
    override fun getBytes(): ByteArray {
        val extensionLength = 2 + namedGroups.size * 2
        val buffer = Buffer()
        buffer.writeShort(ExtensionType.SUPPORTED_GROUPS.value)
        buffer.writeShort(extensionLength.toShort()) // Extension data length (in bytes)

        buffer.writeShort((namedGroups.size * 2).toShort())
        for (namedGroup in namedGroups) {
            buffer.writeShort(namedGroup.value)
        }
        require(buffer.size.toInt() == 4 + extensionLength)
        return buffer.readByteArray()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SupportedGroupsExtension

        return namedGroups.contentEquals(other.namedGroups)
    }

    override fun hashCode(): Int {
        return namedGroups.contentHashCode()
    }


    companion object {
        fun createSupportedGroupsExtension(namedGroup: NamedGroup): SupportedGroupsExtension {
            val namedGroups = arrayOf(namedGroup)
            return SupportedGroupsExtension(namedGroups)
        }


        fun parse(buffer: Buffer, extensionLength: Int): SupportedGroupsExtension {
            val namedGroups: MutableList<NamedGroup> = ArrayList()
            val extensionDataLength = validateExtensionHeader(
                buffer, extensionLength, 2 + 2
            )
            val namedGroupsLength = buffer.readShort().toInt()
            if (extensionDataLength != 2 + namedGroupsLength) {
                throw DecodeErrorException("inconsistent length")
            }
            if (namedGroupsLength % 2 != 0) {
                throw DecodeErrorException("invalid group length")
            }

            var i = 0
            while (i < namedGroupsLength) {
                val namedGroupBytes = (buffer.readShort() % 0xffff).toShort()
                val namedGroup: NamedGroup = NamedGroup.get(namedGroupBytes)
                namedGroups.add(namedGroup)
                i += 2
            }

            return SupportedGroupsExtension(namedGroups.toTypedArray())
        }
    }
}
