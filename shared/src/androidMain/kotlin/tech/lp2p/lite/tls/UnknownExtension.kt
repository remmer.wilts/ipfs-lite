package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

data class UnknownExtension(val type: Int, val data: ByteArray) : Extension {
    override fun getBytes(): ByteArray {
        return ByteArray(0)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UnknownExtension

        if (type != other.type) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = type
        result = 31 * result + data.contentHashCode()
        return result
    }

    companion object {

        fun parse(buffer: Buffer, extensionType: Int, extensionLength: Int): UnknownExtension {

            if (buffer.size < extensionLength) {
                throw DecodeErrorException("Invalid extension length")
            }
            val data = buffer.readByteArray(extensionLength)

            return UnknownExtension(extensionType, data)
        }
    }
}
