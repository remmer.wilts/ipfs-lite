package tech.lp2p.odin

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.room.Room
import androidx.room.RoomDatabase
import tech.lp2p.odin.data.Files
import tech.lp2p.odin.data.Peers


fun databasePeers(ctx: Context): RoomDatabase.Builder<Peers> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath("peers.db")
    return Room.databaseBuilder<Peers>(
        context = appContext,
        name = dbFile.absolutePath
    )
}

fun createPeers(ctx: Context) : Peers {
    return peersDatabaseBuilder(databasePeers(ctx))
}


fun databaseFiles(ctx: Context): RoomDatabase.Builder<Files> {
    val appContext = ctx.applicationContext
    val dbFile = appContext.getDatabasePath("files.db")
    return Room.databaseBuilder<Files>(
        context = appContext,
        name = dbFile.absolutePath
    )
}

fun createFiles(ctx: Context) : Files {
    return filesDatabaseBuilder(databaseFiles(ctx))
}

fun createDataStore(context: Context): DataStore<Preferences> = createDataStore(
    producePath = { context.filesDir.resolve(dataStoreFileName).absolutePath }
)
