package tech.lp2p.asen


data class Key(val hash: ByteArray, val target: ByteArray) {
    init {
        require(hash.size == HASH_LENGTH) { "hash size must be 32" }
    }


    override fun hashCode(): Int {
        var result = hash.contentHashCode()
        result = 31 * result + target.contentHashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Key

        if (!hash.contentEquals(other.hash)) return false
        if (!target.contentEquals(other.target)) return false

        return true
    }

}