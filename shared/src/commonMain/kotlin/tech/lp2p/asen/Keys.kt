package tech.lp2p.asen


// Note a peerId is always a public key (ed25519)

data class Keys(val peerId: PeerId, val privateKey: ByteArray) {


    override fun hashCode(): Int {
        var result = peerId.hashCode()
        result = 31 * result + privateKey.contentHashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Keys

        if (peerId != other.peerId) return false
        if (!privateKey.contentEquals(other.privateKey)) return false

        return true
    }
}

