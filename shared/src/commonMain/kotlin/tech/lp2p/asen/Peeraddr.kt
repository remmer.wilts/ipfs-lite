package tech.lp2p.asen

import kotlinx.io.Buffer
import kotlinx.io.readByteArray
import kotlinx.io.writeUShort
import kotlin.experimental.or

data class Peeraddr(val peerId: PeerId, val address: ByteArray, val port: UShort) {
    init {
        require(address.size == 4 || address.size == 16) { "Invalid size for address" }
        require(port >= 0.toUShort()) { "Invalid port" }
    }

    private fun numericInet4(): String {
        return (address[0].toInt() and 255).toString() + "." +
                (address[1].toInt() and 255) + "." +
                (address[2].toInt() and 255) + "." + (address[3].toInt() and 255)
    }

    private fun holder(): Int {
        var holder: Int = address[3].toInt() and 0xFF
        holder = holder or ((address[2].toInt() shl 8) and 0xFF00)
        holder = holder or ((address[1].toInt() shl 16) and 0xFF0000)
        holder = holder or ((address[0].toInt() shl 24) and -0x1000000)
        return holder
    }

    @OptIn(ExperimentalStdlibApi::class)
    private fun numericInet6(): String {
        val builder = StringBuilder(39)

        for (var2 in 0..7) {
            builder.append(
                (
                        address[var2 shl 1].toInt() shl 8 and '\uff00'.code or
                                (address[(var2 shl 1) + 1].toInt() and 255)
                        ).toHexString()
            )
            if (var2 < 7) {
                builder.append(":")
            }
        }

        return builder.toString()
    }

    fun address(): String {
        if (inet4()) {
            return numericInet4()
        }
        return numericInet6()
    }

    fun encoded(): ByteArray {
        Buffer().use { buffer ->
            if (inet4()) { // IP4
                encodeProtocol(IP4, buffer)
            } else {
                encodeProtocol(IP6, buffer)
            }
            encodePart(address, buffer)
            encodeProtocol(UDP, buffer)
            encodePart(port, buffer)
            return buffer.readByteArray()
        }
    }

    fun inet4(): Boolean {
        return address.size == 4
    }

    fun inet6(): Boolean {
        return address.size == 16
    }


    private fun putUvarint(buf: ByteArray, value: Long) {
        var x = value
        var i = 0
        while (x >= 0x80) {
            buf[i] = (x or 0x80L).toByte()
            x = x shr 7
            i++
        }
        buf[i] = x.toByte()
    }

    private fun encodeProtocol(code: Int, out: Buffer) {
        val varint = ByteArray((32 - code.countLeadingZeroBits() + 6) / 7)
        putUvarint(varint, code.toLong())
        out.write(varint)
    }

    private fun encodePart(port: UShort, buffer: Buffer) {
        buffer.writeUShort(port)
    }

    private fun encodePart(address: ByteArray, buffer: Buffer) {
        buffer.write(address)
    }


    fun isLanAddress(): Boolean {
        return isAnyLocalAddress
                || isLinkLocalAddress
                || isLoopbackAddress
                || isSiteLocalAddress
    }

    override fun hashCode(): Int {
        var result = peerId.hashCode()
        result = 31 * result + address.contentHashCode()
        result = 31 * result + port.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Peeraddr

        if (peerId != other.peerId) return false
        if (!address.contentEquals(other.address)) return false
        if (port != other.port) return false

        return true
    }


    val isAnyLocalAddress: Boolean
        get() {
            if (inet6()) {
                var test: Byte = 0x00
                for (i in 0..15) {
                    test = test.toByte() or address[i].toByte()
                }
                return (test.toInt() == 0x00)
            } else {
                return holder() == 0
            }
        }

    val isLoopbackAddress: Boolean
        get() {
            if (inet6()) {
                var test: Byte = 0x00
                for (i in 0..14) {
                    test = test.toByte() or address[i].toByte()
                }
                return (test.toInt() == 0x00) && (address[15].toInt() == 0x01)
            } else {
                return address[0].toInt() == 127
            }
        }

    val isLinkLocalAddress: Boolean
        get() {
            if (inet6()) {
                return ((address[0].toInt() and 0xff) == 0xfe
                        && (address[1].toInt() and 0xc0) == 0x80)
            } else {


                // link-local unicast in IPv4 (169.254.0.0/16)
                // defined in "Documenting Special Use IPv4 Address Blocks
                // that have been Registered with IANA" by Bill Manning
                // draft-manning-dsua-06.txt
                val address: Int = holder()
                return (((address ushr 24) and 0xFF) == 169)
                        && (((address ushr 16) and 0xFF) == 254)
            }
        }


    val isSiteLocalAddress: Boolean
        get() {
            if (inet6()) {
                return ((address[0].toInt() and 0xff) == 0xfe
                        && (address[1].toInt() and 0xc0) == 0xc0)
            } else {
                // refer to RFC 1918
                // 10/8 prefix
                // 172.16/12 prefix
                // 192.168/16 prefix
                val address: Int = holder()
                return (((address ushr 24) and 0xFF) == 10)
                        || ((((address ushr 24) and 0xFF) == 172)
                        && (((address ushr 16) and 0xF0) == 16))
                        || ((((address ushr 24) and 0xFF) == 192)
                        && (((address ushr 16) and 0xFF) == 168))
            }
        }

}