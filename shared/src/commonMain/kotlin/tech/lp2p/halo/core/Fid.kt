package tech.lp2p.halo.core

import tech.lp2p.halo.MAX_CHARS_SIZE
import tech.lp2p.halo.Node


internal data class Fid(
    private val cid: Long,
    private val size: Long,
    private val name: String,
    private val mimeType: String,
    private val links: List<Long>
) : Node {

    fun links(): List<Long> {
        return links
    }

    override fun size(): Long {
        return size
    }

    override fun name(): String {
        return name
    }

    override fun mimeType(): String {
        return mimeType
    }

    override fun cid(): Long {
        return cid
    }

    init {
        require(size >= 0) { "Invalid size" }
        require(name.length <= MAX_CHARS_SIZE) { "Invalid name length" }
    }
}

