package tech.lp2p.odin.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [FileInfo::class], version = 1, exportSchema = false)
abstract class Files : RoomDatabase() {
    abstract fun filesDao(): FilesDao

    suspend fun fileNames(): List<String> {
        return filesDao().names()
    }

    suspend fun storeFileInfo(fileInfo: FileInfo): Long {
        return filesDao().insertFileInfo(fileInfo)
    }

    suspend fun done(idx: Long, cid: Long) {
        filesDao().done(idx, cid)
    }

    suspend fun delete(idx: Long) {
        filesDao().delete(idx)
    }

}


