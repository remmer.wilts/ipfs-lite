package tech.lp2p.odin.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import kotlinx.coroutines.flow.first
import tech.lp2p.asen.PeerId
import tech.lp2p.asen.PeerStore
import tech.lp2p.asen.Peeraddr

@Database(entities = [Peer::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class Peers : RoomDatabase(), PeerStore {
    abstract fun bootstrapDao(): PeerDao

    override suspend fun peeraddrs(limit: Int): List<Peeraddr> {
        return bootstrapDao().randomPeers(limit).first().map { peer -> peer.peeraddr() }
    }

    override suspend fun storePeeraddr(peeraddr: Peeraddr) {
        bootstrapDao().insertPeer(Peer(peeraddr.peerId, peeraddr.encoded()))
    }
}


object Converters {

    @TypeConverter
    fun toPeerId(data: ByteArray): PeerId {
        return PeerId(data)
    }

    @TypeConverter
    fun toArray(peerId: PeerId): ByteArray {
        return peerId.hash
    }
}