package tech.lp2p.odin.model

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.byteArrayPreferencesKey
import androidx.datastore.preferences.core.edit
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


private val privateKey = byteArrayPreferencesKey("privateKey")
private val publicKey = byteArrayPreferencesKey("publicKey")


suspend fun setPublicKey(dataStore: DataStore<Preferences>, key: ByteArray) {
    dataStore.edit { settings ->
        settings[publicKey] = key
    }
}

fun getPublicKey(dataStore: DataStore<Preferences>): Flow<ByteArray> =
    dataStore.data.map { settings ->
        settings[publicKey] ?: byteArrayOf()
    }

suspend fun setPrivateKey(dataStore: DataStore<Preferences>, key: ByteArray) {
    dataStore.edit { settings ->
        settings[privateKey] = key
    }
}

fun getPrivateKey(dataStore: DataStore<Preferences>): Flow<ByteArray> =
    dataStore.data.map { settings ->
        settings[privateKey] ?: byteArrayOf()
    }

