package tech.lp2p.odin

import androidx.room.Room
import androidx.room.RoomDatabase
import tech.lp2p.odin.data.Files


fun databaseFiles(): RoomDatabase.Builder<Files> {
    val dbFilePath = documentDirectory() + "/files.db"
    return Room.databaseBuilder<Files>(
        name = dbFilePath,
    )
}



private fun documentDirectory(): String {
    /*
    val documentDirectory = NSFileManager.defaultManager.URLForDirectory(
        directory = NSDocumentDirectory,
        inDomain = NSUserDomainMask,
        appropriateForURL = null,
        create = false,
        error = null,
    )
    return requireNotNull(documentDirectory?.path)
    */
    TODO("no apple available")
}


/*
fun createDataStore(): DataStore<Preferences> = createDataStore(

    producePath = {
        val documentDirectory: NSURL? = NSFileManager.defaultManager.URLForDirectory(
            directory = NSDocumentDirectory,
            inDomain = NSUserDomainMask,
            appropriateForURL = null,
            create = false,
            error = null,
        )
        requireNotNull(documentDirectory).path + "/$dataStoreFileName"
    }
)*/